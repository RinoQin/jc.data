(function (root, factory) {
  // Browser globals
  root.jichengSearchEngine = factory();
}(this, function () {

'use strict';

const URL_PARAMS_SCHEMA = {
  index: {name: 'bookListPages', type: 'string[]'},
  bsep: {name: 'bookListSeparator', type: 'string'},
  sep: {name: 'searchSegmentSeparator', type: 'string'},
  othres: {name: 'openConfirmBookThreshold', type: 'integer'},
  thres: {name: 'searchConfirmBookThreshold', type: 'integer'},
  synonym: {name: 'synonymListFiles', type: 'string[]'},
  nopunc: {name: 'noPuncRegex', type: 'string'},
};

const SUPPORT_REGEXP_UNICODE_PROPERTY_ESCAPES = (() => {
  try {
    new RegExp('\\p{L}\\p{N}', 'u');
  } catch (ex) {
    return false;
  }
  return true;
})();

const jichengSearchEngine = {
  conf: {
    bookListPages: ['index.html'],
    bookListSeparator: '·',
    openConfirmBookThreshold: 2,
    searchConfirmBookThreshold: 20,
    searchSegmentSeparator: ' » ',
    synonymListFiles: ['../_common/synonyms.json'],
    synonyms: {},
    noPuncRegex: SUPPORT_REGEXP_UNICODE_PROPERTY_ESCAPES ?
      '[^\\p{L}\\p{N}]' :
      '[' + [
        '\\s\\x00-\\x2F\\x3A-\\x40\\x5B-\\x60',
        '\\u2000-\\u27FF\\u2900-\\u2BFF',
        '\\u3001\\u3002\\u3008-\\u3021\\u3190-\\u319F\\u3200-\\u33FF\\u4DC0-\\u4DFF',
        '\\uFE10-\\uFE6F',
        '\\uFF00-\\uFF0F\\uFF1A-\\uFF20\\uFF3B-\\uFF40\\uFF5B-\\uFF65',
        '\\uFFE0-\\uFFFF',
      ].join('') + ']',
    excludeSelectorBase: 'style, script, noscript, form, template, frame, iframe',
    excludeSelector: '',
  },

  get synonymList() {
    this.initSynonymList();
    return this.synonymList;
  },

  get synonymMaxLen() {
    this.initSynonymList();
    return this.synonymMaxLen;
  },

  initSynonymList(synonyms = this.conf.synonyms) {
    const synonymsEscaped = {};
    let synonymsEscapedMaxLen = -Infinity;

    Object.keys(synonyms).forEach((key) => {
      const keys = synonyms[key].slice();
      keys.unshift(key);
      key = jicheng.escapeRegExp(key);
      synonymsEscapedMaxLen = Math.max(synonymsEscapedMaxLen, key.length);
      synonymsEscaped[key] = keys.map(jicheng.escapeRegExp);
    });

    Object.defineProperty(this, 'synonymList', {value: synonymsEscaped});
    Object.defineProperty(this, 'synonymMaxLen', {value: Math.max(synonymsEscapedMaxLen, 1)});
  },

  /**
   * Main
   */
  async init() {
    // 初始化 GUI
    const header = document.querySelector('#search-wrapper header');
    header.removeAttribute('class');
    header.textContent = `正在載入檢索引擎...`;

    // 載入 URL 參數
    {
      const params = jicheng.loadUrlParams({}, URL_PARAMS_SCHEMA);

      // special validation and handling for several params
      if (typeof params.bookListPages !== 'undefined') {
        params.bookListPages = params.bookListPages.reduce((list, value) => {
          if (!value) {
            return list.concat(this.conf.bookListPages);
          }
          list.push(value);
          return list;
        }, []);
      }
      if (typeof params.synonymListFiles !== 'undefined') {
        params.synonymListFiles = params.synonymListFiles.reduce((list, value) => {
          if (!value) {
            return list.concat(this.conf.synonymListFiles);
          }
          list.push(value);
          return list;
        }, []);
      }
      if (typeof params.openConfirmBookThreshold !== 'undefined') {
        // take care of NaN
        if (!(params.openConfirmBookThreshold >= 0)) {
          delete params.openConfirmBookThreshold;
        }
      }
      if (typeof params.searchConfirmBookThreshold !== 'undefined') {
        // take care of NaN
        if (!(params.searchConfirmBookThreshold >= 0)) {
          delete params.searchConfirmBookThreshold;
        }
      }

      Object.assign(this.conf, params);
    }

    // 載入書籍列表
    try {
      const wrapper = document.querySelector('#search-panel-books');

      for (let url of this.conf.bookListPages) {
        let doc;
        try {
          doc = (await jicheng.xhr({
            url,
            responseType: 'document',
            overrideMimeType: 'text/html;charset=utf8',
          })).response;
        } catch (ex) {
          throw new Error(`network request for '${url}' failed.`);
        }

        if (!doc) {
          throw new Error(`document at '${url}' is null.`);
        }

        const indexname = (this.conf.bookListPages.length > 1) ? (doc.title || url) : '';
        let subWrapper = wrapper;
        for (const elem of doc.querySelectorAll('[data-type="booklist"] a[href]')) {
          const stack = [];

          let desc = null;
          let parent = elem.closest('li');
          if (parent) {
            if (parent.querySelector('ol, ul')) {
              stack.unshift(elem.textContent);
              if (desc === null) {
                desc = elem.getAttribute('aria-description');
              }
            }
            while (true) {
              parent = parent.closest('ol, ul');
              if (!parent) { break; }
              parent = parent.closest('li');
              if (!parent) { break; }
              let p = parent.closest('ol, ul');
              if (!p) { break; }
              p = p.closest('[data-type="booklist"]');
              if (!p) { break; }
              if (p.closest('svg, math')) { break; }

              const a = parent.querySelector('a');
              if (a && a.parentNode === parent) {
                stack.unshift(a.textContent);
                if (desc === null) {
                  desc = a.getAttribute('aria-description');
                }
              }
            }
          }

          if (indexname) {
            stack.unshift(indexname);
          }

          const group = stack.join(this.conf.bookListSeparator);
          if (group) {
            if (group !== subWrapper.label) {
              subWrapper = wrapper.appendChild(document.createElement('optgroup'));
              subWrapper.label = group;
              if (desc) {
                subWrapper.setAttribute('aria-description', desc);
                subWrapper.title = desc;
              }
            }
          } else {
            subWrapper = wrapper;
          }

          const opt = subWrapper.appendChild(document.createElement('option'));
          opt.value = elem.href;
          opt.textContent = elem.textContent || elem.href;
        }
      }

      header.textContent = `本檢索引擎會把選取的典籍即時下載至本機做精確條件比對，可能佔用較多網路流量及本機資源。`;

      const panel = document.querySelector('#search-panel');
      panel.hidden = false;
    } catch (ex) {
      header.className = 'error';
      header.setAttribute('role', 'alert');
      header.setAttribute('aria-live', 'assertive');
      header.textContent = `錯誤：無法載入典籍列表：${ex.message}`;
      return;
    }

    // 載入異體字列表
    try {
      const synonyms = {};
      for (let url of this.conf.synonymListFiles) {
        let json;
        try {
          json = (await jicheng.xhr({
            url,
            responseType: 'json',
            overrideMimeType: 'application/json',
          })).response;
        } catch (ex) {
          throw new Error(`unable to load '${url}': ${ex.message}`);
        }
        Object.assign(synonyms, json);
      }
      this.conf.synonyms = synonyms;
    } catch (ex) {
      header.className = 'error';
      header.setAttribute('role', 'alert');
      header.setAttribute('aria-live', 'assertive');
      header.textContent = `錯誤：無法載入異體字列表：${ex.message}`;
      return;
    }
  },

  filterBooks() {
    let query;
    try {
      query = new KeywordQuery({
        query: document.querySelector('#search-panel-filterbook-keyword').value,
        useSynonyms: document.querySelector('#search-panel-synonyms').checked,
        noPunc: document.querySelector('#search-panel-noPunc').checked,
        ignoreCase: !document.querySelector('#search-panel-case').checked,
      });
    } catch (ex) {
      console.error(ex);
      return;
    }
    console.log(`篩選: ${query.query} => ${query.condition}`);

    const booksWrapper = document.querySelector('#search-panel-books');
    for (const elem of booksWrapper.querySelectorAll('option')) {
      elem.hidden = elem.disabled = query.condition && !query.match(elem.textContent);
    }
    for (const elem of booksWrapper.querySelectorAll('optgroup')) {
      elem.hidden = elem.disabled = !elem.querySelector('option:not([hidden])');
    }
  },

  filterBooksByResults() {
    const filter = new Set(
      Array.prototype.map.call(
        document.querySelectorAll('#search-results > details > summary > a:first-child'),
        el => el.textContent
      )
    );

    if (filter.size === 0) {
      console.error(`目前沒有檢索結果，篩選典籍中止。`);
      return;
    }

    const booksWrapper = document.querySelector('#search-panel-books');
    for (const elem of booksWrapper.querySelectorAll('option')) {
      elem.selected = filter.has(elem.textContent);
    }
  },

  filterBooksSelectAll() {
    const booksWrapper = document.querySelector('#search-panel-books');
    for (const elem of booksWrapper.querySelectorAll('option:not([hidden])')) {
      elem.selected = true;
    }
  },

  filterBooksSelectNone() {
    const booksWrapper = document.querySelector('#search-panel-books');
    for (const elem of booksWrapper.querySelectorAll('option:not([hidden])')) {
      elem.selected = false;
    }
  },

  mapSelectedBooks(selectElem) {
    const books = new Map();
    for (const elem of selectElem.querySelectorAll('option')) {
      if (!elem.selected) {
        continue;
      }

      const url = elem.value;
      if (books.has(url)) {
        continue;
      }

      const name = elem.textContent;
      books.set(url, name);
    }
    return books;
  },

  async search() {
    const startTime = Date.now();

    // 將 #search-results 換成另一個元素，使之前的檢索中斷
    const resultWrapperOrig = document.querySelector('#search-results');
    const resultWrapper = resultWrapperOrig.cloneNode(false);
    resultWrapperOrig.replaceWith(resultWrapper);

    // 建立檢索結果主標
    const header = resultWrapper.appendChild(document.createElement('header'));
    header.setAttribute('role', 'alert');
    header.setAttribute('aria-live', 'polite');
    header.setAttribute('aria-atomic', 'true');
    header.setAttribute('aria-busy', 'true');
    header.textContent = `檢索準備中...`;

    const handleError = (msg) => {
      header.className = 'error';
      header.setAttribute('aria-live', 'assertive');
      header.setAttribute('aria-busy', 'false');
      header.textContent = msg;
    };

    // 建立要檢索的書籍列表
    // 若無書則中止
    const books = this.mapSelectedBooks(document.querySelector('#search-panel-books'));
    if (books.size <= 0) {
      handleError(`錯誤：請選取要檢索的典籍。`);
      return;
    }

    // 建立比對及標示關鍵詞用的 RegExp
    // 若發生錯誤則中止
    let qText;
    {
      const query = document.querySelector('#search-panel-keyword').value;
      if (query.trim()) {
        try {
          qText = new KeywordQuery({
            query,
            useSynonyms: document.querySelector('#search-panel-synonyms').checked,
            noPunc: document.querySelector('#search-panel-noPunc').checked,
            ignoreCase: !document.querySelector('#search-panel-case').checked,
          });
        } catch (ex) {
          handleError(`錯誤：${ex.message}`);
          return;
        }
        console.log(`檢索: ${qText.query} => ${qText.condition}`);
      }
    }

    let qChapter;
    {
      const query = document.querySelector('#search-panel-metadata-chapter').value;
      if (query.trim()) {
        try {
          qChapter = new KeywordQuery({
            query,
            useSynonyms: document.querySelector('#search-panel-synonyms').checked,
            noPunc: document.querySelector('#search-panel-noPunc').checked,
            ignoreCase: !document.querySelector('#search-panel-case').checked,
          });
        } catch (ex) {
          handleError(`錯誤：${ex.message}`);
          return;
        }
        console.log(`檢索 (章節): ${qChapter.query} => ${qChapter.condition}`);
      }
    }

    let qAuthor;
    {
      const query = document.querySelector('#search-panel-metadata-author').value;
      if (query.trim()) {
        try {
          qAuthor = new KeywordQuery({
            query,
            useSynonyms: document.querySelector('#search-panel-synonyms').checked,
            noPunc: document.querySelector('#search-panel-noPunc').checked,
            ignoreCase: !document.querySelector('#search-panel-case').checked,
          });
        } catch (ex) {
          handleError(`錯誤：${ex.message}`);
          return;
        }
        console.log(`檢索 (作者): ${qAuthor.query} => ${qAuthor.condition}`);
      }
    }

    let qDate;
    {
      const query = document.querySelector('#search-panel-metadata-date').value;
      if (query.trim()) {
        try {
          qDate = new DateQuery({
            query,
            useSynonyms: document.querySelector('#search-panel-synonyms').checked,
            ignoreCase: !document.querySelector('#search-panel-case').checked,
          });
        } catch (ex) {
          handleError(`錯誤：${ex.message}`);
          return;
        }
        console.log(`檢索 (年份): ${qDate.query} => ${qDate.min} ~ ${qDate.max}`);
      }
    }

    let qQuality;
    {
      const query = document.querySelector('#search-panel-metadata-quality').value;
      if (query.trim()) {
        try {
          qQuality = new QualityQuery({
            query,
            useSynonyms: document.querySelector('#search-panel-synonyms').checked,
            ignoreCase: !document.querySelector('#search-panel-case').checked,
          });
        } catch (ex) {
          handleError(`錯誤：${ex.message}`);
          return;
        }
        console.log(`檢索 (品質): ${qQuality.query} => ${qQuality.min} ~ ${qQuality.max}`);
      }
    }

    // 選取的典籍太多時，向使用者確認
    if (books.size >= this.conf.searchConfirmBookThreshold) {
      if (!confirm(`檢索 ${books.size} 部典籍可能佔用大量網路流量及系統資源，確定要繼續嗎？`)) {
        header.setAttribute('aria-busy', 'false');
        header.textContent = `檢索已取消。`;
        return;
      }
    }

    // 初始化其他選項
    const searchUnit = document.querySelector('#search-panel-unit').value;

    const useAncient = document.querySelector('#search-panel-useAncient').checked;
    const useZhu = document.querySelector('#search-panel-useZhu').checked;
    const useShu = document.querySelector('#search-panel-useShu').checked;
    const useJiao = document.querySelector('#search-panel-useJiao').checked;
    const useDing = document.querySelector('#search-panel-useDing').checked;
    const useExcludeSelector = document.querySelector('#search-panel-useExcludeSelector').checked;

    const showSummary = document.querySelector('#search-panel-showSummary').checked;
    const expandAll = document.querySelector('#search-panel-expandAll').checked;

    // 建立略過元素的 selector
    let removeElemSelector = [this.conf.excludeSelectorBase];
    let removeWrapperSelector = [];
    {
      if (!useAncient) {
        removeElemSelector.push('[data-rev="古版"]');
        removeWrapperSelector.push('[data-rev="古版-元素"]');
      } else {
        removeElemSelector.push('[data-rev="今版"]');
        removeWrapperSelector.push('[data-rev="今版-元素"]');
      }

      if (!useZhu) {
        removeElemSelector.push('span[data-rev="注"]');
      }

      if (!useShu) {
        removeElemSelector.push('span[data-rev="疏"]');
      }

      if (!useJiao) {
        removeElemSelector.push('span[data-rev="校"]');
      }

      if (!useDing) {
        removeElemSelector.push('span[data-rev="訂"]:not([data-rev="*"])');
      } else {
        removeElemSelector.push('span[data-rev="訂"][data-ver]');
      }

      if (useExcludeSelector && jichengSearchEngine.conf.excludeSelector) {
        removeElemSelector.push(jichengSearchEngine.conf.excludeSelector);
      }

      removeElemSelector = removeElemSelector.join(', ');
      removeWrapperSelector = removeWrapperSelector.join(', ');
    }

    // 準備新的搜尋工作
    const searchHitHeaderSegmentsGenerator = () => {
      const me = searchHitHeaderSegmentsGenerator;
      return me.sectionStack.map((elem, index) => {
        let label;
        if (me.sectionIsParagraph && index === me.sectionStack.length - 1) {
          label = '#' + (me.sectionIndex + 1)
        } else {
          const elemClone = elem.cloneNode(true);
          for (const elem of elemClone.querySelectorAll('[data-sec=""]')) {
            elem.remove();
          }
          label = elemClone.textContent.trim();
        }
        const xpath = me.mapRefXpath.get(elem);
        return {label, xpath};
      });
    };

    const searchHitHeaderGenerator = ({
      bookName, bookUrl,
      segments = [],
      separator = this.conf.searchSegmentSeparator,
    }) => {
      const params = new URLSearchParams();
      if (searchUnit !== 'source') {
        params.append('useAncient', useAncient);
        params.append('useZhu', useZhu ? 'diff' : 'reject');
        params.append('useShu', useShu ? 'diff' : 'reject');
        params.append('useJiao', useJiao ? 'diff' : 'reject');
        params.append('useDing', useDing ? 'accept' : 'reject');
        if (qText) {
          for (const pattern of qText.highlightRegexes) {
            params.append('hl', pattern.toString());
          }
        }
      }

      const urlObj = new URL(bookUrl);
      const query = params.toString();
      urlObj.hash = query ? '?' + query : '';

      const breadcrumbs = document.createDocumentFragment();
      const anchor = breadcrumbs.appendChild(document.createElement('a'));
      anchor.href = urlObj.href;
      anchor.target = '_blank';
      anchor.textContent = bookName;

      let started = false;
      for (const segment of segments) {
        if (!segment) {
          if (started) {
            breadcrumbs.appendChild(document.createTextNode(separator));
          }
          continue;
        }

        started = true;
        const {label, xpath} = segment;
        params.set('t', xpath);
        const query = params.toString();
        urlObj.hash = query ? '?' + query : '';

        breadcrumbs.appendChild(document.createTextNode(separator));
        const anchor = breadcrumbs.appendChild(document.createElement('a'));
        anchor.href = urlObj.href;
        anchor.target = '_blank';
        anchor.textContent = label;
      }

      if (qChapter) {
        jicheng.mark(breadcrumbs, qChapter.highlightRegexes);
      }

      return breadcrumbs;
    };

    const searchHitHandler = ({
      elem,
      bookName, bookUrl,
      segmentsGenerator,
    }) => {
      let text = elem.textContent;

      if (qText) {
        if (!qText.match(text)) { return false; }
      }

      const item = document.createElement('details');
      const header = item.appendChild(document.createElement('summary'));
      header.appendChild(searchHitHeaderGenerator({
        bookName, bookUrl,
        segments: typeof segmentsGenerator === 'function' ? segmentsGenerator() : undefined,
      }));

      const div = item.appendChild(document.createElement('div'));
      if (showSummary) {
        let snippets = [];
        let offset = 0;
        let end = 0;
        let len = text.length;

        if (qText) {
          // 建立比對關鍵詞的 RegExp
          // 若關鍵詞含有 regex capture group 則不使用 re2, re3，以免衝突出錯
          const src = qText.highlightRegex.source;
          const flags = qText.highlightRegex.flags;
          const re1 = qText.highlightRegex;
          const re2 = qText.hasCaptureGroupKeyword ? /^(?!)/ : new RegExp(`(${src}).{0,75}(?!\\1)${src}`, flags);
          const re3 = qText.hasCaptureGroupKeyword ? /^(?!)/ : new RegExp(`(${src}).{0,45}(?!\\1)(${src}).{0,45}(?!\\1)(?!\\2)${src}`, flags);

          for (let cnt = 4; cnt--;) {
            re1.lastIndex = re2.lastIndex = re3.lastIndex = offset;
            const match = re3.exec(text) || re2.exec(text) || re1.exec(text);
            if (!match) { break; }

            const str = match[0];
            const strlen = str.length;
            const idx = match.index;

            // 建立前後文，取匹配字串前後共 100 字
            // 若一側不足 50 字則加長另一側
            let pre = Math.min(idx - offset, 100);
            let post = Math.min(len - idx - strlen, 100);

            if (pre > 50 && post > 50) {
              pre = post = 50;
            } else if (pre > 50) {
              pre = Math.min(pre, 100 - post);
            } else if (post > 50) {
              post = Math.min(post, 100 - pre);
            } else if (offset === 0) {
              // 兩側都不足 50 字，直接使用整個字串，略過複雜的全文片段演算法
              snippets = [text];
              break;
            }

            // 計算起始點及結束點。如有重疊則加到上一個片段
            const start = idx - pre;
            const append = (start < end) ? end : false;  // 開始點在上一片段結尾前，則加到上一片段
            end = idx + strlen + post;  // 現在將結束點設為此片段後

            if (append) {
              snippets[snippets.length - 1] += text.substring(append, end);
            } else {
              snippets.push(text.substring(start, end));
            }

            // 設定 offset 以決定比對下一個關鍵詞的起始點
            offset = idx + strlen;
          }
        } else {
          snippets = [text.slice(0, 100)];
        }

        div.textContent = snippets.join('…');
        div.className = 'snippets';
      } else {
        div.setAttribute('data-render', 'book');
        div.classList.add('main');

        switch (elem.nodeType) {
          case 1:
            div.innerHTML = elem.innerHTML;
            break;
          case 11:
            div.appendChild(elem);
            break;
          default:
            console.error(new Error(`Unsupported node type: ${elem.nodeName}`));
            break;
        }
      }

      // 標示關鍵詞
      if (qText) {
        jicheng.mark(div, qText.highlightRegexes, {
          acrossElements: true,
        });
      }

      resultWrapper.appendChild(item);
      resultCountElem.textContent = parseInt(resultCountElem.textContent, 10) + 1;

      jicheng.registerRenderingElem(div, true);
      item.addEventListener('toggle', onToggleResult);
      item.open = expandAll; // can fire the toggle event
    };

    const textHtmlHighlighter = (text, regex, markStart, markEnd) => {
      let rv = '';
      let hits = 0;
      let m;
      let lastIndex = 0;
      regex.lastIndex = 0;
      while (m = regex.exec(text)) {
        hits++;
        const mpre = text.slice(lastIndex, m.index);
        const mtext = text.slice(m.index, regex.lastIndex);
        rv += jicheng.escapeHtml(mpre, {noDoubleQuotes: true}) + markStart + jicheng.escapeHtml(mtext, {noDoubleQuotes: true}) + markEnd;
        if (regex.lastIndex === lastIndex) {
          regex.lastIndex += 1;
        }
        lastIndex = regex.lastIndex;
      }
      rv += jicheng.escapeHtml(text.slice(lastIndex), {noDoubleQuotes: true});
      return {html: rv, hits};
    };

    const searchSourceHitHandler = ({
      elem,
      bookName, bookUrl,
    }) => {
      let text = elem.innerHTML.replace(/^\n/, '');

      if (qText) {
        if (!qText.match(text)) { return false; }
      }

      const item = document.createElement('details');
      item.open = expandAll;
      const header = item.appendChild(document.createElement('summary'));
      header.appendChild(searchHitHeaderGenerator({bookName, bookUrl}));

      const div = item.appendChild(document.createElement('div'));
      const pre = div.appendChild(document.createElement('pre'));
      pre.className = 'source';

      const tagStart = '<mark data-markjs="true">';
      const tagEnd = '</mark>';
      let highlight;
      if (qText) {
        highlight = textHtmlHighlighter(text, qText.highlightRegex, tagStart, tagEnd);
        keywordHits += highlight.hits;
      }

      if (showSummary) {
        div.className = 'snippets';
        if (highlight) {
          const regex1 = new RegExp(jicheng.escapeRegExp(tagStart));
          const regex2 = new RegExp('^(?:(?!' + jicheng.escapeRegExp(tagStart) + ').)*?' + jicheng.escapeRegExp(tagEnd));
          const regex3 = new RegExp(jicheng.escapeRegExp(tagStart) + '(?:(?!' + jicheng.escapeRegExp(tagEnd) + ').)*?$');

          let html = '';
          highlight.html.split('\n').forEach((line, index) => {
            if (!regex1.test(line)) { return; }
            if (regex2.test(line)) {
              line = tagStart + line;
            }
            if (regex3.test(line)) {
              line = line + tagEnd;
            }
            html += `<b>行${index + 1}: </b>${line}\n`;
          });
          pre.innerHTML = html;
        }
      } else {
        if (highlight) {
          pre.innerHTML = highlight.html;
        } else {
          pre.textContent = text;
        }
      }

      resultWrapper.appendChild(item);
      resultCountElem.textContent = parseInt(resultCountElem.textContent, 10) + 1;
    };

    const searchBookDoc = async ({
      doc,
      bookName, bookUrl,
    }) => {
      // 根元素
      const mainElem = doc.querySelector('main');

      // 篩選元資料
      const metaElem = mainElem.querySelector('header > dl.元資料');
      const meta = metaElem ? jicheng.loadDlKeyValue(metaElem) : null;

      if (qAuthor) {
        if (!meta || !meta['作者'] || !meta['作者'].some(author => qAuthor.match(author))) { return; }
      }

      if (qDate) {
        if (!meta || !meta['年份'] || !meta['年份'].some(date => qDate.match(date))) { return; }
      }

      if (qQuality) {
          const q = meta && meta['品質'] || ['-1%'];
          if (!q.some(quality => qQuality.match(quality))) { return; }
      }

      // 為參考節點建立對照資訊
      let refSelector = '[data-sec="h1"], [data-sec="h2"], [data-sec="h3"], [data-sec="h4"], [data-sec="h5"], [data-sec="h6"]';
      switch (searchUnit) {
        case 'paragraph':
          refSelector += ', [data-sec="p"]';
          break;
      }

      // 記錄各節點在原始 HTML 中的 XPath
      const mapRefXpath = new WeakMap();
      if (['chapter', 'paragraph'].includes(searchUnit)) {
        const mapChildInfo = new WeakMap();
        mapChildInfo.set(mainElem, new Map());
        mapRefXpath.set(mainElem, '');

        const nodes = function* () {
          const nodeIterator = doc.createNodeIterator(mainElem, NodeFilter.SHOW_ELEMENT);
          nodeIterator.nextNode(); // 略過 mainElem
          let node;
          while (node = nodeIterator.nextNode()) {
            yield node;
          }
        };
        await jicheng.forEachAsync(nodes(), (elem) => {
          const curNodeName = elem.nodeName;
          const parentElem = elem.parentNode;
          const childInfo = mapChildInfo.get(parentElem);

          // update sibling element conut for the parent
          const pos = (childInfo.get(curNodeName) || 0) + 1;
          childInfo.set(curNodeName, pos);

          // get XPath
          const parentXpath = mapRefXpath.get(parentElem);
          const xpath = parentXpath ? `${parentXpath}/${curNodeName}[${pos}]` : `${curNodeName}[${pos}]`;

          mapChildInfo.set(elem, new Map());
          mapRefXpath.set(elem, xpath);
        });
      }

      // 更新佔位元素使之能在檢索結果正確顯示
      jicheng.toggleHiddenContent(mainElem, '[data-jc-innerhtml]');
      jicheng.toggleUnwrappedElems(mainElem, 'jc-t', null, (oldElem, newElem) => {
        mapRefXpath.set(newElem, mapRefXpath.get(oldElem));
      });

      // 移除不檢索的元素
      await jicheng.forEachAsync(
        mainElem.querySelectorAll(removeElemSelector),
        (elem) => {
          elem.remove();
        },
      );

      await jicheng.forEachAsync(
        mainElem.querySelectorAll(removeWrapperSelector),
        (elem) => {
          let child;
          while (child = elem.firstChild) {
            elem.before(child);
          }
          elem.remove();
        },
      );

      // 針對指定的搜尋單位做比對，並輸出結果
      switch (searchUnit) {
        case 'book': {
          searchHitHandler({
            elem: mainElem,
            bookName, bookUrl,
          });
          break;
        }

        case 'chapter':
        case 'paragraph': {
          const mapOrigClone = new WeakMap();
          const sectionStack = [];
          let sectionFragment = document.createDocumentFragment();
          let sectionIndex = 0;
          let sectionIsParagraph = false;

          const searchFragment = () => {
            // 比對章節關鍵詞
            if (qChapter) {
              if (!sectionStack.some((elem, index) => {
                if (sectionIsParagraph && index === sectionStack.length - 1) {
                  return false;
                }
                return qChapter.match(elem.textContent.trim());
              })) {
                return;
              }
            }

            Object.assign(searchHitHeaderSegmentsGenerator, {
              sectionStack,
              sectionFragment,
              sectionIndex,
              sectionIsParagraph,
              mapRefXpath,
            });

            searchHitHandler({
              elem: sectionFragment,
              bookName, bookUrl,
              segmentsGenerator: searchHitHeaderSegmentsGenerator,
            });
          };

          const nodesGenerator = function* () {
            const nodeIterator = document.createNodeIterator(mainElem, NodeFilter.SHOW_ALL);
            nodeIterator.nextNode(); // 略過 mainElem
            let node;
            while (node = nodeIterator.nextNode()) {
              yield node;
            }
          };

          await jicheng.forEachAsync(nodesGenerator(), (node) => {
            // handle sectioning element
            if (node.nodeType === 1 && node.matches(refSelector)
                && !(node.matches('[data-sec="p"]') && node.parentNode.closest(refSelector))) {
              // 建立新 sectionFragment
              searchFragment(sectionFragment, sectionIndex);
              sectionFragment = document.createDocumentFragment();
              sectionIndex++;

              // 更新 sectionStack
              if (sectionIsParagraph) {
                sectionStack.pop();
              }
              sectionIsParagraph = !/^h(\d+)$/i.test(node.getAttribute('data-sec'));
              if (!sectionIsParagraph) {
                const level = parseInt(RegExp.$1, 10);
                sectionStack[level - 1] = node;
                sectionStack.length = level;
              } else {
                sectionStack.push(node);
              }

              // 為 node 及其祖節點建立複本並加入目前的 sectionFragment
              mapOrigClone.set(node, node.cloneNode(false));
              let current = node;
              let parent = current.parentNode;
              while (parent && parent !== mainElem) {
                const parentClone = parent.cloneNode(false);
                parentClone.appendChild(mapOrigClone.get(current));
                mapOrigClone.set(parent, parentClone);
                current = parent;
                parent = current.parentNode;
              }
              sectionFragment.appendChild(mapOrigClone.get(current));

              return;
            }

            // 建立複本並加入其 parent 的複本（或目前的 sectionFragment）
            mapOrigClone.set(node, node.cloneNode(false));
            const parent = node.parentNode;
            if (parent === mainElem) {
              sectionFragment.appendChild(mapOrigClone.get(node));
            } else {
              mapOrigClone.get(parent).appendChild(mapOrigClone.get(node));
            }
          });

          searchFragment();

          break;
        }

        case 'source': {
          searchSourceHitHandler({
            elem: mainElem,
            bookName, bookUrl,
          });
          break;
        }
      }
    };

    const onToggleResult = (event) => {
      const details = event.target;
      if (!details.open) { return; }
      const div = details.querySelector('div');
      jicheng.showRenderingElem(div, {
        useAncient,
        useZhu: useZhu ? 'diff' : 'reject',
        useShu: useShu ? 'diff' : 'reject',
        useJiao: useJiao ? 'diff' : 'reject',
        useDing: useDing ? 'accept' : 'reject',
      });
    };

    // 換成中止按鈕
    document.querySelector('#search-panel-start').hidden = true;
    document.querySelector('#search-panel-cancel').hidden = false;

    // 顯示開始檢索的訊息
    header.textContent = '';
    header.appendChild(document.createTextNode(`正在檢索第`));
    const bookCountElem = header.appendChild(document.createElement('span'));
    bookCountElem.textContent = '0';
    header.appendChild(document.createTextNode(`/`));
    const bookTotalElem = header.appendChild(document.createElement('span'));
    bookTotalElem.textContent = books.size;
    header.appendChild(document.createTextNode(`部典籍，已找到`));
    const resultCountElem = header.appendChild(document.createElement('span'));
    resultCountElem.textContent = '0';
    header.appendChild(document.createTextNode(`筆結果…`));

    // 依次載入各頁面做檢索
    let keywordHits = 0;
    for (const [url, name] of books) {
      try {
        // 檢查檢索是否已中斷
        if (!resultWrapper.isConnected) { break; }

        bookCountElem.textContent = parseInt(bookCountElem.textContent, 10) + 1;

        let doc;
        try {
          doc = (await jicheng.xhr({
            url,
            responseType: 'document',
            overrideMimeType: 'text/html',
          })).response;
          if (!doc) {
            throw new Error(`document for '${name}' is null.`);
          }
        } catch (ex) {
          const details = resultWrapper.appendChild(document.createElement('details'));
          const summary = details.appendChild(document.createElement('summary'));
          summary.className = 'error';
          summary.textContent = `錯誤：無法載入典籍「${name}」`;
          throw new Error(`unable to retrieve document for '${name}'.`);
        }

        await searchBookDoc({
          doc,
          bookName: name,
          bookUrl: url,
        });
      } catch (ex) {
        console.error(ex);
      }
    }

    // 檢索結束，更新主標
    const time = (Date.now() - startTime) / 1000;
    header.setAttribute('aria-busy', 'false');
    if (searchUnit === 'source' && qText) {
      header.textContent = `從${bookCountElem.textContent}部典籍中找到${resultCountElem.textContent}筆結果（${keywordHits}處符合的關鍵詞），費時${time}秒。`;
    } else {
      header.textContent = `從${bookCountElem.textContent}部典籍中找到${resultCountElem.textContent}筆結果，費時${time}秒。`;
    }

    // 檢查檢索是否已中斷
    if (!resultWrapper.parentNode) { return; }

    // 回復介面
    document.querySelector('#search-panel-start').hidden = false;
    document.querySelector('#search-panel-cancel').hidden = true;
  },

  viewBooks() {
    const books = this.mapSelectedBooks(document.querySelector('#search-panel-books'));

    // 選取的典籍太多時，向使用者確認
    if (books.size >= this.conf.openConfirmBookThreshold) {
      if (!confirm(`確定要開啟 ${books.size} 部典籍檢視？`)) {
        return;
      }
    }

    for (const [url, name] of books) {
      window.open(url);
    }
  },

  cancel() {
    // 將 #search-results 換成另一個元素，使檢索中斷
    const resultWrapperOrig = document.querySelector('#search-results');
    const resultWrapper = resultWrapperOrig.cloneNode(false);
    let child;
    while (child = resultWrapperOrig.firstChild) {
      resultWrapper.appendChild(child);
    }
    resultWrapperOrig.replaceWith(resultWrapper);

    // 回復介面
    document.querySelector('#search-panel-start').hidden = false;
    document.querySelector('#search-panel-cancel').hidden = true;
  },
};

class KeywordQuery {
  constructor({query, ignoreCase = true, multiline = true, useSynonyms = true, useRegex = false, noPunc = false}) {
    const handleKeyword = (keyword) => {
      let key = keyword;
      if (useRegex) {
        if (!neg) {
          captureGroupRegex.lastIndex = 0;
          while (captureGroupRegex.test(key)) {
            if (RegExp.$1) {
              hasCaptureGroupKeyword = true;
              break;
            }
          }
        }
        try {
          key = new RegExp(key, regexFlag).source;
        } catch(ex) {
          console.error(`Bad query: ${keyword} => /${key}/${regexFlag}`);
          throw new Error(`正規表示式「${keyword}」語法有誤。`);
        }
      } else {
        key = jicheng.escapeRegExp(key);
      }
      if (useSynonyms) { key = KeywordQuery.propagateSynonyms(key); }
      if (noPunc) { key = KeywordQuery.propagateNoPunc(key); }
      if (!key) { key = '(?:)'; }  // prevent error in case key becomes empty
      neg ? neg = false : highlightPatterns.push(key);
      q.push('/' + key + '/' + regexFlag + '.test(text)');
    };

    const handlePrefixedKeyword = (query) => {
      if (/^(-*)([a-z]+):(.*?)$/i.test(query)) {
        const positive = RegExp.$1.length % 2 === 0;
        const cmd = RegExp.$2;
        query = RegExp.$3;
        switch (cmd) {
          case 're': {
            useRegex = positive;
            break;
          }
          case 'syn': {
            useSynonyms = positive;
            break;
          }
          case 'np': {
            noPunc = positive;
            break;
          }
        }
        if (neg) { neg = false; }
      } else {
        while (query[0] === '-') {
          q.push('!');
          neg = !neg;
          query = query.slice(1);
        }
      }

      if (!query.length) { return; }

      handleKeyword(query);
    };

    const fixerRegex = /((?:^|\s+)AND(?=\s|$))|((?:^|\s+)OR(?=\s|$))|(\s*\()|(\s*\))|(\s+)|"([^"]*(?:""[^"]*)*)"/gui;
    const captureGroupRegex = new RegExp([
      '\\\\(?:k<[^>]*>|.)',
      '\\[[^\\\\\\]]*(?:\\\\.[^\\\\\\]]*)*\\]',
      '(\\((?:(?!\\?)|\\?<[^>]*>))',
    ].join('|'), 'gu');
    const regexFlag = (ignoreCase ? 'i' : '') + (multiline ? 'm' : '') + 'u';
    let q = [], m, i = 0, delta, neg = false;
    let highlightPatterns = [];
    let hasCaptureGroupKeyword = false;

    while ((m = fixerRegex.exec(query))) {
      delta = query.substring(i, m.index).trim();
      handlePrefixedKeyword(delta);

      if (m[1] !== undefined) {
        q.push('&&');
      } else if (m[2] !== undefined) {
        q.push('||');
      } else if (m[3] !== undefined) {
        q.push('(');
      } else if (m[4] !== undefined) {
        q.push(')');
      } else if (m[5] !== undefined) {
        // do nothing
      } else if (m[6] !== undefined) {
        handleKeyword(m[6].replace(/""/g, '"'));
      }

      i = fixerRegex.lastIndex;
    }

    delta = query.substring(i).trim();
    handlePrefixedKeyword(delta);

    for (i = 0; i < q.length - 1; i++) {
      if (/^[\/)]/.test(q[i]) && /^[\/(!]/.test(q[i+1])) {
        q[i] += ' &&';
      }
    }

    q = q.join(' ');

    // 驗證語法是否正確
    try {
      const text = '';
      eval(q);
    } catch(ex) {
      console.error(`Bad query: ${query} => ${q}`);
      throw new Error(`檢索條件「${query}」語法有誤。`);
    }

    this.query = query;
    this.condition = q;
    this.highlightRegex = new RegExp(highlightPatterns.join('|'), regexFlag + 'g');
    this.highlightRegexes = highlightPatterns.map(pattern => new RegExp(pattern, regexFlag + 'g'));
    this.hasCaptureGroupKeyword = hasCaptureGroupKeyword;
  }

  match(text) {
    return eval(this.condition);
  }

  /**
   * 將 regexStr 中的字串取代為可比對任一異體字
   *
   * - 略過 [...]，因 [A] 轉為 [(?:A1|A2)] 會造成語義錯誤
   * - 略過 \ooo, \xhh, \uhhhh, \cX, \X，因無原始字元，無法與轉換表正常比對
   * - 保留 "\" + 原始字元 的跳脫形式
   */
  static propagateSynonyms(...args) {
    const singleCharRegex = /^\(\?:[^|)](?:\|[^|)])*\)$/u;

    // 除了跳脫的字元 \. 以外，篩除非關鍵字的部分，以免被誤轉換
    const fixerRegex = new RegExp([
      '\\\\(?:u(?:[0-9A-Fa-f]{4}|\\{[0-9A-Fa-f]+\\})|x[0-9A-Fa-f]{2}|[0-3][0-7]{0,2}|[4-7][0-7]{0,1}|c[A-Za-z]|[pP]\\{[A-Za-z]+\\}|k<[^>]*>|[A-Za-z])',
      '(\\\\.)', // group 1
      '\\(\\?(?:[:!=]|<(?:[=!]|[^>]*>))',
      '\\[[^\\\\\\]]*(?:\\\\.[^\\\\\\]]*)*\\]',
      '\\{\\d+(?:,\\d*)?\\}',
      '[/^$*+?.|()[\\]{}]',
    ].join('|'), 'gu');

    const replaceWithSynonymList = (str) => {
      const output = [];
      outer:
      for (let i = 0, I = str.length; i < I; i++) {
        let s = str.substr(i, synonymMaxLen);
        for (let j = synonymMaxLen; j >= 1; j--) {
          s = s.substr(0, j);
          let v = synonymList[s];
          if (v) {
            s = `(?:${v.join('|')})`;
            if (singleCharRegex.test(s)) { s = `[${v.join('')}]`; }
            output.push(s);
            i += j - 1;
            continue outer;
          }
        }
        output.push(s);
      }
      return output.join('');
    };

    let synonymList;
    let synonymMaxLen;

    const fn = this.propagateSynonyms = (regexStr) => {
      synonymList = jichengSearchEngine.synonymList;
      synonymMaxLen = jichengSearchEngine.synonymMaxLen;
      const result = [];
      let lastIndex = fixerRegex.lastIndex = 0;
      while (fixerRegex.test(regexStr)) {
        const m = RegExp.lastMatch;
        if (RegExp.$1) { continue; }
        const delta = regexStr.substring(lastIndex, fixerRegex.lastIndex - m.length);
        result.push(replaceWithSynonymList(delta));
        result.push(m);
        lastIndex = fixerRegex.lastIndex;
      }
      const delta = regexStr.substring(lastIndex);
      result.push(replaceWithSynonymList(delta));
      return result.join('');
    };

    return fn(...args);
  }

  /**
   * 將 regexStr 中的字串取代為忽略標點（或字元）
   */
  static propagateNoPunc(...args) {
    // 除了跳脫的字元 \. 以外，其餘部分非關鍵字，中間不加 sep
    const fixerRegex = new RegExp([
      '\\\\(?:u(?:[0-9A-Fa-f]{4}|\\{[0-9A-Fa-f]+\\})|x[0-9A-Fa-f]{2}|[0-3][0-7]{0,2}|[4-7][0-7]{0,1}|c[A-Za-z]|[pP]\\{[A-Za-z]+\\}|k<[^>]*>|[A-Za-z])',
      '(\\\\.)', // group 1
      '\\(\\?(?:[:!=]|<(?:[=!]|[^>]*>))',
      '\\[[^\\\\\\]]*(?:\\\\.[^\\\\\\]]*)*\\]',
      '\\{\\d+(?:,\\d*)?\\}',
      '[/^$*+?.|()[\\]{}]',
    ].join('|'), 'gu');

    const unescaperRegex = /\\(\.)/g;

    const insertSep = (str) => {
      let s = str;
      s = s.replace(unescaperRegex, '$1');
      s = s.replace(noPuncRegex, '');
      s = Array.from(s).map(jicheng.escapeRegExp).join(noPuncSep);
      return s;
    };

    let noPuncRegex;
    let noPuncSep;

    const fn = this.propagateNoPunc = (regexStr) => {
      noPuncRegex = new RegExp(jichengSearchEngine.conf.noPuncRegex, 'migu');
      noPuncSep = '(?:' + jichengSearchEngine.conf.noPuncRegex + ')*';
      const stack = [];
      let lastIndex = fixerRegex.lastIndex = 0;
      while (fixerRegex.test(regexStr)) {
        const m = RegExp.lastMatch;
        if (RegExp.$1) { continue; }
        const delta = regexStr.substring(lastIndex, fixerRegex.lastIndex - m.length);
        if (delta) { stack.push(insertSep(delta)); }
        stack.push(m);
        lastIndex = fixerRegex.lastIndex;
      }
      const delta = regexStr.substring(lastIndex);
      if (delta) { stack.push(insertSep(delta)); }

      // "*"、"+"、"?"、"{" 前不可加 sep
      // (..x|y|z) 視為一單位，故 "(.." 後、"|" 前後、")" 前可省略 sep
      const result = [];
      for (let i = stack.length; i--;) {
        if (["(", "|"].includes(stack[i][0])) {
          result[0] = stack[i] + result[0];
        } else if (result.length && [")", "|", "*", "+", "?", "{"].includes(result[0][0])) {
          result[0] = stack[i] + result[0];
        } else {
          result.unshift(stack[i]);
        }
      }

      return result.join(noPuncSep);
    };

    return fn(...args);
  }
}

class DateQuery {
  constructor({query}) {
    if (!/^(-?\d*)(\/?)(-?\d*)$/.test(query)) {
      throw new Error(`年份「${query}」語法有誤。`);
    }

    this.query = query;

    if (RegExp.$2.length) {
      this.min = RegExp.$1.length ? parseInt(RegExp.$1, 10) : -Infinity;
      this.max = RegExp.$3.length ? parseInt(RegExp.$3, 10) : Infinity;
    } else {
      this.min = this.max = parseInt(RegExp.$1, 10);
    }

    if (this.max < this.min) {
      [this.min, this.max] = [this.max, this.min];
    }
  }

  match(text) {
    if (!/^(-?\d{0,4})(?:[^\/]*)(\/?)(-?\d{0,4})(?:[^\/]*)$/.test(text)) { return false; }

    let min, max;
    if (RegExp.$2.length) {
      min = RegExp.$1.length ? parseInt(RegExp.$1, 10) : -Infinity;
      max = RegExp.$3.length ? parseInt(RegExp.$3, 10) : Infinity;
    } else {
      min = max = parseInt(RegExp.$1, 10);
    }

    if (min > max) { return false; }

    return this.min <= max && min <= this.max;
  }
}

class QualityQuery {
  constructor({query}) {
    if (!/^(-?\d*)(\/?)(-?\d*)$/.test(query)) {
      throw new Error(`品質「${query}」語法有誤。`);
    }

    this.query = query;

    if (RegExp.$2.length) {
      this.min = RegExp.$1.length ? parseInt(RegExp.$1, 10) : -Infinity;
      this.max = RegExp.$3.length ? parseInt(RegExp.$3, 10) : Infinity;
    } else {
      this.min = this.max = parseInt(RegExp.$1, 10);
    }

    if (this.max < this.min) {
      [this.min, this.max] = [this.max, this.min];
    }
  }

  match(text) {
    if (/^(-?\d+)%$/.test(text)) {
      const value = parseInt(RegExp.$1);
      return this.min <= value && value <= this.max;
    }
    return false;
  }
}

document.querySelector('#search-panel-filterbook-selectAll').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksSelectAll();
});

document.querySelector('#search-panel-filterbook-selectNone').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksSelectNone();
});

document.querySelector('#search-panel-form-range').addEventListener('submit', (event) => {
  event.preventDefault();
  jichengSearchEngine.filterBooks();
});

document.querySelector('#search-panel-synonymsList').addEventListener('click', (event) => {
  event.preventDefault();
  const title = `請設定新的異體字表 (JSON)：`;
  let value = prompt(title, JSON.stringify(jichengSearchEngine.conf.synonyms));
  let data;
  while (true) {
    if (value === null) {
      return;  // cancel
    }
    try {
      data = JSON.parse(value);

      // validate data
      if (!(typeof data === 'object' && data !== null && !Array.isArray(data))) {
        throw new Error(`輸入資料須為一物件。`);
      }

      for (const [key, value] of Object.entries(data)) {
        if (!(Array.isArray(value) && value.every(x => typeof x === 'string'))) {
          throw new Error(`輸入資料含有非字串陣列的鍵值："${key}": ${JSON.stringify(value)}`);
        }
      }

      break;
    } catch (ex) {
      alert(`輸入格式錯誤：${ex.message}`);
      value = prompt(title, value);
    }
  }

  try {
    jichengSearchEngine.initSynonymList(data);
    jichengSearchEngine.conf.synonyms = data;
  } catch (ex) {
    console.error(ex);
    alert(`無法修改異體字表：${ex.message}`);
  }
});

document.querySelector('#search-panel-noPuncList').addEventListener('click', (event) => {
  event.preventDefault();
  const title = `請設定要略過的標點符號字元（正規表示式）：`;
  const value = prompt(title, jichengSearchEngine.conf.noPuncRegex);
  while (true) {
    if (value === null) {
      return;  // cancel
    }
    try {
      const regex = new RegExp(value, 'migu');
      jichengSearchEngine.conf.noPuncRegex = value;
      break;
    } catch (ex) {
      alert(`正規表示式語法錯誤：${ex.message}`);
      value = prompt(title, value);
    }
  }
});

document.querySelector('#search-panel-excludeSelector').addEventListener('click', (event) => {
  event.preventDefault();
  const title = `請設定要移除的網頁元素（CSS選擇器）：`;
  const value = prompt(title, jichengSearchEngine.conf.excludeSelector);
  while (true) {
    if (value === null) {
      return;  // cancel
    }
    try {
      document.querySelector(value);
      jichengSearchEngine.conf.excludeSelector = value;
      break;
    } catch (ex) {
      alert(`CSS選擇器語法錯誤：${ex.message}`);
      value = prompt(title, value);
    }
  }
});

document.querySelector('#search-panel-form-settings').addEventListener('submit', (event) => {
  event.preventDefault();
  jichengSearchEngine.search();
});

document.querySelector('#search-panel-cancel').addEventListener('click', (event) => {
  jichengSearchEngine.cancel();
});

document.querySelector('#search-panel-filterBookByResults').addEventListener('click', (event) => {
  jichengSearchEngine.filterBooksByResults();
});

document.querySelector('#search-panel-viewBooks').addEventListener('click', (event) => {
  jichengSearchEngine.viewBooks();
});

document.addEventListener('DOMContentLoaded', (event) => {
  jichengSearchEngine.init();
});

return jichengSearchEngine;

}));
