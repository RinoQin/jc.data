這是[中醫笈成](https://jicheng.tw/tcm/index.html)網站的原始檔，可透過[笈成站台產生器](https://gitlab.com/jicheng/jc.ssg)建立靜態網站。

# 外掛字體檔案
* 預設在使用者選擇加載字體時，會嘗試使用系統安裝的字體，若未安裝則自動從[笈成檢字系統](https://jicheng.tw/hanzi)下載。
* 若要自行管理字體檔案，可下載以下檔案放在 `themes/default/static/_common/fonts/`：
  * `HanaMinA.ttf`, `HanaMinB.ttf`: https://fonts.jp/hanazono/
  * `TW-Sung-98_1.ttf`, `TW-Sung-Ext-B-98_1.ttf`, `TW-Kai-98_1.ttf`, `TW-Kai-Ext-B-98_1.ttf`: https://data.gov.tw/dataset/5961
