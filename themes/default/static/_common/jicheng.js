(function (root, factory) {
  // Browser globals
  root.jicheng = factory();
}(this, function () {

'use strict';

/**
 * 常數
 */
const ASCII_WHITESPACE = /[ \t\n\r\f]/;

const URL_PARAMS_SCHEMA = {
  t: {name: 'locate', type: 'string'},
  hl: {name: 'highlight', type: 'string[]'},
  currentTab: {type: 'string'},
  useAncient: {type: 'string'},
  useZhu: {type: 'string'},
  useShu: {type: 'string'},
  useJiao: {type: 'string'},
  useDing: {type: 'string'},
  useVertical: {type: 'string'},
  useFont: {type: 'string'},
  useFontCustom: {type: 'string'},
  fontSize: {type: 'float'},
  useHanzi: {type: 'string'},
  prettify: {type: 'string'},
  tocMode: {type: 'string'},
};

const URL_PARAMS_HANDLER = {
  'string': x => x,
  'integer': x => parseInt(x, 10),
  'float': x => parseFloat(x),
};

const SYNCED_OPTIONS = [
  'currentTab',
  'useAncient',
  'useZhu',
  'useShu',
  'useJiao',
  'useDing',
  'useVertical',
  'useFont',
  'useFontCustom',
  'fontSize',
  'useHanzi',
  'prettify',
  'tocMode',
];

const jicheng = {
  options: {
    currentTab: "page-panel-tabpanel-option",
    useAncient: "false",
    useZhu: "auto",
    useShu: "auto",
    useJiao: "auto",
    advancedAnnotationFilter: null,
    useDing: "auto",
    advancedVersionFilter: null,
    useVertical: "auto",
    useFont: "auto",
    useFontCustom: "",
    fontSize: 16,
    useHanzi: "auto",
    prettify: "auto",
    tocMode: "section",
  },

  mainElem: null,
  mainElemSectioned: null,
  renderingElems: new Set(),
  runtimeOptions: {},
  lastTouchElement: null,
  titleElems: [],
  mapTitleElemToTocElem: new Map(),

  /**
   * 輔助及共用函數
   */
  setIsEqual(setA, setB) {
    if (setA.size !== setB.size) { return false; }
    for (const i of setA) {
      if (!setB.has(i)) { return false; }
    }
    return true;
  },

  escapeHtml(...args) {
    const regex = /[&<>"']| (?= )/g;
    const func = m => map[m];
    const map = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;"
    };
    const fn = jicheng.escapeHtml = (str, {
      noDoubleQuotes = false,
      singleQuotes = false,
      spaces = false,
    } = {}) => {
      map['"'] = noDoubleQuotes ? '"' : "&quot;";
      map["'"] = singleQuotes ? "&#39;" : "'";
      map[" "] = spaces ? "&nbsp;" : " ";
      return str.replace(regex, func);
    };
    return fn(...args);
  },

  escapeRegExp(str) {
    // 不脫義 "-"，因 input[pattern] 不允許脫義 "-"
    // Escaping "-" allows the result to be inserted into a character class.
    // Escaping "/" allow the result to be used in a JS regex literal.
    const regex = /[/\\^$*+?.|()[\]{}]/g;
    const fn = jicheng.escapeRegExp = (str) => {
      return str.replace(regex, "\\$&");
    };
    return fn(str);
  },

  tidyCssPropertyValue(...args) {
    const dummyElem = document.createElement('span');

    const tidyCssPropertyValue = this.tidyCssPropertyValue = (propertyName, value) => {
      // the style property won't change if value is invalid,
      // and the got value will generally become "".
      dummyElem.style.removeProperty(propertyName);
      dummyElem.style.setProperty(propertyName, value);
      return dummyElem.style.getPropertyValue(propertyName);
    };

    return tidyCssPropertyValue(...args);
  },

  /**
   * @param {WeakMap} map - 用快取加速取得大量連續的 Xpath，但 refNode 和 DOM 皆不可變動
   */
  getXPath(node, refNode, map) {
    const regex = /\[(\d+)\]$/;

    const getXPathInternal = (node, refNode, map) => {
      if (!node || node === refNode) {
        return '';
      }

      let xpath;
      let name = node.nodeName;
      let i = 0;
      let sibling = node;
      while (sibling) {
        if (sibling.nodeName === name) {
          if (map && (xpath = map.get(sibling))) {
            if (sibling === node) {
              return xpath;
            }

            xpath = xpath.replace(regex, (_, idx) => `[${parseInt(idx, 10) + i}]`);
            map.set(node, xpath);
            return xpath;
          }
          i++;
        }
        sibling = sibling.previousSibling;
      }

      xpath = getXPathInternal(node.parentNode, refNode);
      switch (node.nodeType) {
        case 1:
          xpath += `/${name}[${i}]`;
          break;
        case 3:
          xpath += `/text()[${i}]`;
          break;
        case 8:
          xpath += `/comment()[${i}]`;
          break;
        default:
          throw new Error(`Unsupported node type: ${node.nodeName}".`);
      }
      if (xpath[0] === '/') { xpath = xpath.slice(1); }

      map && map.set(node, xpath);
      return xpath;
    };

    const fn = jicheng.getXPath = (node, refNode, map) => {
      if (!node) {
        return null;
      }

      if (node === refNode) {
        return '.';
      }

      return getXPathInternal(node, refNode, map);
    };

    return fn(node, refNode, map);
  },

  /**
   * 按 WHATWG 規範解析 dl 元素的鍵值組
   *
   * ref: https://html.spec.whatwg.org/multipage/grouping-content.html#the-dl-element
   */
  parseDl(elem) {
    const processDtDd = (elem) => {
      switch (elem.nodeName.toLowerCase()) {
        case 'dt':
          if (seenDd) {
            groups.push(current);
            current = {name: [], value: []};
            seenDd = false;
          }
          current.name.push(elem);
          break;
        case 'dd':
          current.value.push(elem);
          seenDd = true;
          break;
      }
    };

    const groups = [];
    let current = {name: [], value: []};
    let seenDd = false;
    let child = elem.firstChild;
    let grandchild = null;

    while (child) {
      switch (child.nodeName.toLowerCase()) {
        case 'div':
          grandchild = child.firstChild;
          while (grandchild) {
            processDtDd(grandchild);
            grandchild = grandchild.nextSibling;
          }
          break;
        default:
          processDtDd(child);
          break;
      }
      child = child.nextSibling;
    }

    if (current.name.length || current.value.length) {
      groups.push(current);
    }

    return groups;
  },

  /**
   * 將 dl 元素的鍵值組解析為 JSON 物件
   *
   * - dt, dd 可多對多組合（dl.元資料只支援一對一）
   * - dt, dd 中若有 data[value] 則取其值（多個 data 相當於多個 dt/dd），
   *   否則取 textContent
   *
   * @param {boolean} readRaw - 忽略 data[value] 只讀取 textContent 值
   * @return {Object.<string, Array.<string>>}
   */
  loadDlKeyValue(elem, readRaw = false) {
    const getElemValues = (stack, elem) => {
      const dataElems = elem.querySelectorAll('data[value]');
      if (dataElems.length && !readRaw) {
        for (const dataElem of dataElems) {
          // Google Chrome < 62 does not support HTMLDataElement.value
          stack.push(dataElem.getAttribute('value'));
        }
      } else {
        stack.push(elem.textContent.trim());
      }
      return stack;
    };

    const groups = this.parseDl(elem);
    const metadata = {};
    for (const group of groups) {
      const names = group.name.reduce(getElemValues, []);
      const values = group.value.reduce(getElemValues, []);
      for (const name of names) {
        if (!metadata[name]) {
          metadata[name] = [];
        }
        for (const value of values) {
          metadata[name].push(value);
        }
      }
    }
    return metadata;
  },

  xhr(params = {}) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      if (params.onreadystatechange) {
        xhr.onreadystatechange = function (event) {
          params.onreadystatechange(xhr);
        };
      }

      xhr.onload = function (event) {
        if (xhr.status == 200 || xhr.status == 0) {
          // we only care about real loading success
          resolve(xhr);
        } else {
          // treat "404 Not found" or so as error
          let statusText = xhr.statusText || scrapbook.httpStatusText[xhr.status];
          statusText = xhr.status + (statusText ? " " + statusText : "");
          reject(new Error(statusText));
        }
      };

      xhr.onabort = function (event) {
        // resolve with no param
        resolve();
      };

      xhr.onerror = function (event) {
        // No additional useful information can be get from the event object.
        reject(new Error("Network request failed."));
      };

      xhr.ontimeout = function (event) {
        reject(new Error("Request timeout."));
      };

      xhr.responseType = params.responseType;
      xhr.open(params.method || "GET", params.url, true);

      if (params.overrideMimeType) { xhr.overrideMimeType(params.overrideMimeType); }
      if (params.timeout) { xhr.timeout = params.timeout; }

      xhr.send();
    });
  },

  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  },

  async forEachAsync(iterable, cb, threshold = 100) {
    let t = new Date();
    let i = 0;
    for (let item of iterable) {
      await cb(item, i++, iterable);
      if (new Date() - t > threshold) {
        await this.sleep(0);
        t = new Date();
      }
    }
  },

  /**
   * 主要函數 / 介面相關
   */
  init(event) {
    this.panelElem = document.querySelector('#page-panel');
    this.mainElem = document.querySelector('main');

    this.mainElemSectioned = this.mainElem.cloneNode(false);
    this.mainElemSectioned.className = 'main sectioned';
    this.mainElemSectioned.hidden = true;
    this.mainElem.after(this.mainElemSectioned);

    this.registerRenderingElem(this.mainElem, true);
    this.registerRenderingElem(this.mainElemSectioned, true);

    {
      let elem = document.querySelector('#page-panel-open-wrapper');
      if (elem) {
        elem.hidden = false;
      }
    }

    {
      let elem = document.querySelector('#page-panel-open');
      if (elem) {
        elem.href = "javascript:";
        elem.addEventListener('click', (event) => {
          this.togglePanel(true);
        });
      }
    }

    {
      let elem = document.querySelector('#page-panel-close');
      if (elem) {
        elem.href = "javascript:";
        elem.addEventListener('click', (event) => {
          // special handling when closing options tabpanel
          if (document.getElementById('page-panel-tabpanel-option').getAttribute('aria-expanded') === 'true') {
            this.submitOptions();
          }
          this.togglePanel(false);
        });
      }
    }

    {
      let elem = document.querySelector('#page-panel form');
      if (elem) {
        elem.addEventListener('submit', (event) => {
          event.preventDefault();
          this.submitOptions();
          this.togglePanel(false);
        });
      }
    }

    for (const elem of document.querySelectorAll('[aria-description]')) {
      elem.title = elem.getAttribute('aria-description');
    }

    for (const elem of document.querySelectorAll('#page-panel [role="tab"]')) {
      elem.addEventListener('click', (event) => {
        this.togglePanelTab(event.currentTarget.getAttribute('aria-controls'));
      });
    }

    {
      let elem = document.querySelector('#useFontCustomBtn');
      if (elem) {
        elem.addEventListener('click', (event) => {
          const title = `請設定要使用的字體名稱：
• 若字體名稱含有空白字元，應使用半形雙引號括住。例如：「"Times New Roman"」。
• 指定多個字體時，用半形逗號分隔，中間可加空白字元。例如：「標楷體, 新細明體」。`;
          let value = prompt(title, this.options.useFontCustom);
          if (value === null) {
            return;
          }
          this.options.useFontCustom = value;
        });
      }
    }

    {
      let elem = document.querySelector('#useAdvancedAnnotationBtn');
      if (elem) {
        const ANNOTATION_SELECTOR = [
          '[data-rev="注"][data-ver]',
          '[data-rev="疏"][data-ver]',
          '[data-rev="校"][data-ver]',
        ].join(',');

        const GET_DEFAULT_FILTER = () => {
          const rv = {
            '注': {'': 2},
            '疏': {'': 2},
            '校': {'': 2},
          };
          for (const elem of document.querySelectorAll(ANNOTATION_SELECTOR)) {
            const rev = elem.getAttribute('data-rev');
            const ver = elem.getAttribute('data-ver');
            rv[rev][ver] = 2;
          }
          return rv;
        };

        elem.addEventListener('click', (event) => {
          const title = `進階篩選本書不同來源的注疏校文（JSON，或清空重設）：
• 索引鍵："": 不明或未定義版本的注釋
• 鍵值：0: 拒絕；1: 接受；其他值: 插入
• 鍵值可用 {"value": <狀態值>, "css": {"<屬性>": "<屬性值>", ...}} 的格式自訂樣式`;
          let value = prompt(title, JSON.stringify(this.options.advancedAnnotationFilter || GET_DEFAULT_FILTER()));
          while (true) {
            if (value === null) {
              return;  // cancel
            }
            if (value === '') {
              this.options.advancedAnnotationFilter = null;  // reset
              return;
            }
            try {
              const data = JSON.parse(value);
              if (!(typeof data === 'object' && data !== null && !Array.isArray(data))) {
                throw new Error(`輸入資料須為一物件。`);
              }
              for (const key of ['注', '疏', '校']) {
                if (!(typeof data[key] === 'object' && data[key] !== null && !Array.isArray(data[key]))) {
                  throw new Error(`「${key}」的值須為一物件`);
                }
                for (const ver in data[key]) {
                  if (ASCII_WHITESPACE.test(ver)) {
                    throw new Error(`「${key}」的鍵值「${ver}」錯誤：含有空白字元`);
                  }
                }
              }
              this.options.advancedAnnotationFilter = data;
              break;
            } catch (ex) {
              alert(`物件格式錯誤：${ex.message}`);
              value = prompt(title, value);
            }
          }
        });
      }
    }

    {
      let elem = document.querySelector('#useAdvancedVersionBtn');
      if (elem) {
        const ANNOTATION_SELECTOR = '[data-rev="訂"][data-ver]';

        const GET_DEFAULT_FILTER = () => {
          const rv = {'': 0};
          for (const elem of document.querySelectorAll(ANNOTATION_SELECTOR)) {
            const rev = elem.getAttribute('data-rev');
            const ver = elem.getAttribute('data-ver');
            rv[ver] = 0;
          }
          return rv;
        };

        elem.addEventListener('click', (event) => {
          const title = `進階篩選本書不同來源的訂文（JSON，或清空重設）：
• 索引鍵："": 笈成修訂版的文字；"*": 原典的文字
• 鍵值：0: 拒絕；1: 接受；2: 刪除；其他值: 插入
• 鍵值可用 {"value": <狀態值>, "css": {"<屬性>": "<屬性值>", ...}} 的格式自訂樣式`;
          let value = prompt(title, JSON.stringify(this.options.advancedVersionFilter || GET_DEFAULT_FILTER()));
          while (true) {
            if (value === null) {
              return;  // cancel
            }
            if (value === '') {
              this.options.advancedVersionFilter = null;  // reset
              return;
            }
            try {
              const data = JSON.parse(value);
              if (!(typeof data === 'object' && data !== null && !Array.isArray(data))) {
                throw new Error(`輸入資料須為一物件。`);
              }
              for (const ver in data) {
                if (ASCII_WHITESPACE.test(ver)) {
                  throw new Error(`「${key}」的鍵值「${ver}」錯誤：含有空白字元`);
                }
              }
              this.options.advancedVersionFilter = data;
              break;
            } catch (ex) {
              alert(`物件格式錯誤：${ex.message}`);
              value = prompt(title, value);
            }
          }
        });
      }
    }

    this.loadLocalStorage();
    this.loadOptionsToPanel();
    this.loadUrlParams();
    this.togglePanelTab(this.options['currentTab']);
    this.generateToc();
    this.applyOptions({locate: true});
  },

  loadBookMeta(mainElem) {
    const dlElem = mainElem.querySelector('header > dl[class~="元資料"]');
    if (!dlElem) { return {}; }
    return this.loadDlKeyValue(dlElem);
  },

  togglePanel(willShow) {
    const panel = this.panelElem;
    if (typeof willShow !== "boolean") {
      willShow = !!panel.hidden;
    }
    if (willShow) {
      panel.hidden = false;
      document.querySelector('#page-panel-close').focus();
    } else {
      panel.hidden = true;
    }
  },

  togglePanelTab(toShow) {
    if (document.getElementById(toShow).getAttribute('aria-expanded') !== 'true') {
      // special handling when closing options tabpanel
      if (document.getElementById('page-panel-tabpanel-option').getAttribute('aria-expanded') === 'true') {
        this.submitOptions();
      }

      for (const elem of document.querySelectorAll(`#page-panel [role="tab"]`)) {
        if (elem.getAttribute('aria-controls') === toShow) {
          elem.setAttribute('aria-selected', 'true');
        } else {
          elem.setAttribute('aria-selected', 'false');
        }
      }
      for (const elem of document.querySelectorAll(`#page-panel [role="tabpanel"]`)) {
        if (elem.id === toShow) {
          elem.setAttribute('aria-expanded', 'true');
          elem.hidden = false;
        } else {
          elem.setAttribute('aria-expanded', 'false');
          elem.hidden = true;
        }
      }
    }
    this.options['currentTab'] = toShow;
    this.saveLocalStorage(['currentTab']);
  },

  registerRenderingElem(elem, noRender = false) {
    // 選項改變時觸發
    const onOption = (event) => {
      const wrapper = event.target;
      const options = event.detail.options;

      if (!wrapper.hidden) {
        wrapper.dispatchEvent(new CustomEvent('renderer-beforeshow', {detail: {options: this.runtimeOptions}}));
        wrapper.dispatchEvent(new CustomEvent('renderer-aftershow', {detail: {options: this.runtimeOptions}}));
      }
    };

    // 即將顯示時觸發，此時 wrapper 可能為隱藏狀態（也可能不是）。
    // 主要做不須 computedStyle 等資訊的 DOM 改寫，以減少觸發 reflow/repaint。
    const onBeforeShow = (event) => {
      const wrapper = event.target;
      const options = event.detail.options;

      // record original classes
      const classes0 = new Set(wrapper.classList);
      const classes = new Set(classes0);

      if (wrapper.getAttribute('data-render') === 'book') {
        // 古版
        if (options.useAncient) {
          classes.add('古版');
          if (!classes.has('_古版')) {
            classes.add('_古版');
            this.toggleUnwrappedElems(wrapper, 'jc-t[attr-data-rev="古版-元素"]', '[data-rev="今版-元素"]');
            this.toggleHiddenContent(wrapper, '[data-rev="古版"][data-jc-innerhtml]');
          }
        } else {
          classes.delete('古版');
          if (classes.has('_古版')) {
            classes.delete('_古版');
            this.toggleUnwrappedElems(wrapper, 'jc-t[attr-data-rev="今版-元素"]', '[data-rev="古版-元素"]');
          }
        }

        // 注
        switch (options.useZhu) {
          case "accept":
            classes.add('注-接受');
            classes.delete('注-拒絕');
            break;
          case "reject":
            classes.delete('注-接受');
            classes.add('注-拒絕');
            break;
          case "diff":
          default:
            classes.delete('注-接受');
            classes.delete('注-拒絕');
            break;
        }

        // 疏
        switch (options.useShu) {
          case "accept":
            classes.add('疏-接受');
            classes.delete('疏-拒絕');
            break;
          case "reject":
            classes.delete('疏-接受');
            classes.add('疏-拒絕');
            break;
          case "diff":
          default:
            classes.delete('疏-接受');
            classes.delete('疏-拒絕');
            break;
        }

        // 校
        switch (options.useJiao) {
          case "accept":
            classes.add('校-接受');
            classes.delete('校-拒絕');
            break;
          case "reject":
            classes.delete('校-接受');
            classes.add('校-拒絕');
            break;
          case "diff":
          default:
            classes.delete('校-接受');
            classes.delete('校-拒絕');
            break;
        }

        // 訂
        switch (options.useDing) {
          case "accept":
            classes.add('訂-接受');
            classes.delete('訂-拒絕');
            break;
          case "reject":
            classes.delete('訂-接受');
            classes.add('訂-拒絕');
            break;
          case "diff":
          default:
            classes.delete('訂-接受');
            classes.delete('訂-拒絕');
            break;
        }

        // 進階注疏校篩選
        if (options.advancedAnnotationFilter) {
          for (const key of ['注', '疏', '校']) {
            classes.delete(`${key}-接受`);
            classes.delete(`${key}-拒絕`);
            for (const ver in options.advancedAnnotationFilter[key]) {
              let data = options.advancedAnnotationFilter[key][ver];
              if (data === null || typeof data !== 'object') {
                data = {value: data};
              }
              switch (data.value) {
                case 0:
                  classes.delete(`${key}-${ver}-接受`);
                  classes.add(`${key}-${ver}-拒絕`);
                  break;
                case 1:
                  classes.add(`${key}-${ver}-接受`);
                  classes.delete(`${key}-${ver}-拒絕`);
                  break;
                default:
                  classes.delete(`${key}-${ver}-接受`);
                  classes.delete(`${key}-${ver}-拒絕`);
                  break;
              }
            }
          }
        }

        // 進階訂文篩選
        if (options.advancedVersionFilter) {
          classes.delete(`訂-接受`);
          classes.delete(`訂-拒絕`);
          for (const ver in options.advancedVersionFilter) {
            let data = options.advancedVersionFilter[ver];
            if (data === null || typeof data !== 'object') {
              data = {value: data};
            }
            switch (data.value) {
              case 0:
                classes.delete(`訂-${ver}-接受`);
                classes.add(`訂-${ver}-拒絕`);
                classes.delete(`訂-${ver}-刪除`);
                break;
              case 1:
                classes.add(`訂-${ver}-接受`);
                classes.delete(`訂-${ver}-拒絕`);
                classes.delete(`訂-${ver}-刪除`);
                break;
              case 2:
                classes.delete(`訂-${ver}-接受`);
                classes.delete(`訂-${ver}-拒絕`);
                classes.add(`訂-${ver}-刪除`);
                break;
              default:
                classes.delete(`訂-${ver}-接受`);
                classes.delete(`訂-${ver}-拒絕`);
                classes.delete(`訂-${ver}-刪除`);
                break;
            }
          }
          this.toggleHiddenContent(wrapper, 'span[data-rev="訂"][data-jc-innerhtml]');
        }
      }

      // 標示檢索的關鍵詞
      // 須在 setHanzi 之前，否則無法標示轉成圖片的文字
      if (options.highlight) {
        if (!classes.has('_highlighted')) {
          classes.add('_highlighted');
          this.highlightText(options.highlight, options, wrapper);
        }
      }

      // 精細排版
      if (options.prettify) {
        classes.add('精細排版');
        classes.add('_精細排版');

        // 精細排版可能受其他選項影響，任何選項變化時都要重跑。
        // 先還原以避免二次執行造成錯誤。
        // 因最後仍會做精細排版，還原時不須標記 refreshHanzi。
        onBeforeShowUnprettify(wrapper, true);
        onBeforeShowPrettify(wrapper);
      } else {
        classes.delete('精細排版');
        if (classes.has('_精細排版')) {
          classes.delete('_精細排版');
          onBeforeShowUnprettify(wrapper);
        }
      }

      // apply classes if changed
      if (!this.setIsEqual(classes, classes0)) {
        wrapper.className = [...classes].join(' ');
      }
    };

    // 顯示時觸發。需要 computedStyle 等資訊的 DOM 改寫只能在此時做。
    const onAfterShow = (event) => {
      const wrapper = event.target;
      const options = event.detail.options;

      // record original classes
      const classes0 = new Set(wrapper.classList);
      const classes = new Set(classes0);

      // hanzi
      setHanzi: {
        if (!(jchanzi && jchanzi.processPage)) {
          break setHanzi;
        }

        if (options.useHanzi !== wrapper.getAttribute('data-render-useHanzi')) {
          wrapper.setAttribute('data-render-useHanzi', options.useHanzi);
          jchanzi.unprocessElement(wrapper);
          switch (options.useHanzi) {
            case 'basic':
              jchanzi.conf.convertIdsDynamic = 0;
              jchanzi.processElement(wrapper);
              break;
            case 'advanced':
              jchanzi.conf.convertIdsDynamic = 1;
              jchanzi.processElement(wrapper);
              break;
          }
        } else {
          // 某些情況須重新處理部分子節點
          switch (options.useHanzi) {
            case 'basic':
              jchanzi.conf.convertIdsDynamic = 0;
              for (const elem of wrapper.querySelectorAll('[data-render-prettify-refreshHanzi]')) {
                jchanzi.processElement(elem);
              }
              break;
            case 'advanced':
              jchanzi.conf.convertIdsDynamic = 1;
              for (const elem of wrapper.querySelectorAll('[data-render-prettify-refreshHanzi]')) {
                jchanzi.processElement(elem);
              }
              break;
          }
        }

        for (const elem of wrapper.querySelectorAll('[data-render-prettify-refreshHanzi]')) {
          elem.removeAttribute('[data-render-prettify-refreshHanzi]');
        }
      }

      // apply classes if changed
      if (!this.setIsEqual(classes, classes0)) {
        wrapper.className = [...classes].join(' ');
      }
    };

    const onBeforeShowPrettify = (wrapper) => {
      // 靠右小字, 靠左小字, 略小字
      // 讓每個字確實置中對齊
      {
        for (const elem of wrapper.querySelectorAll('small.組排小字, small.雙行夾注 span.行, small.靠右小字, small.靠左小字, small.略小字')) {
          const nodeIterator = document.createNodeIterator(elem,
            NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_TEXT,
            (node) => node.parentNode.matches('jc-c') ? NodeFilter.FILTER_SKIP : NodeFilter.FILTER_ACCEPT
            );

          let node;
          while (node = nodeIterator.nextNode()) {
            if (node.nodeType === Node.TEXT_NODE) {
              const chars = Array.from(node.data);
              const frag = document.createDocumentFragment();
              for (const char of chars) {
                const elem = frag.appendChild(document.createElement('jc-c'));
                elem.textContent = char;
              }
              // don't use replaceChild, which causes frag be iterated at the next run
              node.before(frag);
              node.remove();
              continue;
            }

            if (node.matches('a[data-jchanzi]')) {
              const wrapper = document.createElement('jc-c');
              node.before(wrapper);
              wrapper.appendChild(node);
              continue;
            }
          }
        }
      }
    };

    const onBeforeShowUnprettify = (wrapper, skipRefresh = false) => {
      // 特定情況可能須重跑 hanzi，故先標記須重新處理缺字的節點
      // 例如以下情況可能導致 IDS 因之前被分開而無法合成：
      // useHanzi: false => prettify: true => useHanzi: advanced => prettify: false
      const refreshHanzi = !skipRefresh && ['basic', 'advanced'].includes(wrapper.getAttribute('data-render-useHanzi'));

      // 靠右小字, 靠左小字, 略小字
      {
        for (const elem of wrapper.querySelectorAll('small.組排小字, small.雙行夾注 span.行, small.靠右小字, small.靠左小字, small.略小字')) {
          for (const node of elem.querySelectorAll('jc-c')) {
            const frag = document.createDocumentFragment();
            let child;
            while (child = node.firstChild) {
              frag.appendChild(child);
            }
            node.replaceWith(frag);
          }
          elem.normalize();

          if (refreshHanzi) {
            elem.setAttribute('data-render-prettify-refreshHanzi', '');
          }
        }
      }
    };

    if (elem.getAttribute('data-render') === 'norender') {
      return;
    }

    if (this.renderingElems.has(elem)) {
      return;
    }

    this.renderingElems.add(elem);
    elem.addEventListener('renderer-option', onOption);
    elem.addEventListener('renderer-beforeshow', onBeforeShow);
    elem.addEventListener('renderer-aftershow', onAfterShow);

    if (!noRender) {
      elem.dispatchEvent(new CustomEvent('renderer-option', {detail: {options: this.runtimeOptions}}));
    }
  },

  showRenderingElem(elem, options = this.runtimeOptions) {
    if (options !== this.runtimeOptions) {
      options = Object.assign({}, this.runtimeOptions, options);
    }
    elem.dispatchEvent(new CustomEvent('renderer-beforeshow', {detail: {options}}));
    elem.hidden = false;
    elem.dispatchEvent(new CustomEvent('renderer-aftershow', {detail: {options}}));
  },

  /**
   * Attempt to submit user options.
   *
   * @throws {Error} if the form doesn't pass validation
   */
  submitOptions() {
    if (!document.querySelector('#page-panel form').reportValidity()) {
      throw new Error('invalid options');
    }
    this.saveOptionsFromPanel();
    this.applyOptions();
    this.saveLocalStorage();
  },

  applyOptions(...args) {
    const cssElem = document.head.appendChild(document.createElement('style'));
    cssElem.classList.add('options');

    const applyOptions = jicheng.applyOptions = ({
      locate = false,
    } = {}) => {
      // update runtime options
      const options0 = Object.assign({}, this.runtimeOptions);
      this.updateRuntimeOptions();
      const options = this.runtimeOptions;

      // skip if options not changed
      if (Object.keys(options).every(k => options[k] === options0[k])) {
        return;
      }

      // get locate target
      let locateTarget;
      let locateTargetToc;
      getLocateTarget: {
        if (!locate) { break getLocateTarget; }

        let target = options['locate'];
        if (!target) { break getLocateTarget; }

        let targetElem;
        try {
          targetElem = document.evaluate(target, this.mainElem, null, XPathResult.ANY_TYPE, null).iterateNext();
          if (!targetElem) { throw new Error(`Matched node is not found`); }
        } catch (ex) {
          console.error(`Unable to locate '${target}': ${ex.message}`);
          break getLocateTarget;
        }

        locateTarget = targetElem.parentNode.insertBefore(document.createElement('jc-locate'), targetElem);
        locateTargetToc = this.getParentSection(targetElem);
      }

      // apply document-level changes
      const htmlClasses0 = new Set(document.documentElement.classList);
      const htmlClasses = new Set(htmlClasses0);
      const styleSheets = [];

      this.setVerticalMode(options.useVertical, htmlClasses);
      this.setAdvancedAnnotationFilter(options.advancedAnnotationFilter, styleSheets);
      this.setAdvancedVersionFilter(options.advancedVersionFilter, styleSheets);
      this.setFontMode(options.fontFamily, styleSheets);
      this.setFontSize(options.fontSize, styleSheets);

      if (!this.setIsEqual(htmlClasses, htmlClasses0)) {
        document.documentElement.className = [...htmlClasses].join(' ');
      }

      const cssText = styleSheets.join('\n');
      if (cssText !== cssElem.textContent) {
        cssElem.textContent = cssText;
      }

      // 分派事件
      for (const elem of this.renderingElems) {
        elem.dispatchEvent(new CustomEvent('renderer-option', {detail: {options}}));
      }

      // 處理 main 以外的其他元素
      setHanzi: {
        if (!(jchanzi && jchanzi.processPage)) {
          break setHanzi;
        }

        const wrappers = [
          document.getElementById('page-header'),
          document.getElementById('page-panel'),
          document.getElementById('page-footer'),
        ];
        if (options.useHanzi !== document.body.getAttribute('data-render-useHanzi')) {
          document.body.setAttribute('data-render-useHanzi', options.useHanzi);
          for (const wrapper of wrappers) {
            jchanzi.unprocessElement(wrapper);
          }
          switch (options.useHanzi) {
            case 'basic':
              jchanzi.conf.convertIdsDynamic = 0;
              for (const wrapper of wrappers) {
                jchanzi.processElement(wrapper);
              }
              break;
            case 'advanced':
              jchanzi.conf.convertIdsDynamic = 1;
              for (const wrapper of wrappers) {
                jchanzi.processElement(wrapper);
              }
              break;
          }
        }
      }

      // 跳轉至目標元素
      // 須在各樣式設定完之後，以便計算正確的捲動位置
      if (locateTarget) {
        this.loadSection(locateTargetToc, {tocOnly: true, closePanel: false});
        this.locateElement(locateTarget);
        locateTarget.remove();
      }
    };

    return applyOptions(...args);
  },

  /**
   * 轉換用 data-jc-innerhtml 隱藏的元素內容
   *
   * - 主要用於避免這些內容被搜尋引擎建索引
   * - 通常在載入文件後只須執行一次
   */
  toggleHiddenContent(wrapper, toShowSelector, toHideSelector, onReplace) {
    if (toShowSelector) {
      for (const elem of wrapper.querySelectorAll(toShowSelector)) {
        elem.innerHTML = elem.getAttribute('data-jc-innerhtml');
        elem.removeAttribute('data-jc-innerhtml');
        onReplace && onReplace(elem);
      }
    }
    if (toHideSelector) {
      for (const elem of wrapper.querySelectorAll(toHideSelector)) {
        elem.setAttribute('data-jc-innerhtml', elem.innerHTML);
        elem.innerHTML = '';
        onReplace && onReplace(elem);
      }
    }
  },

  /**
   * 轉換用 jc-t 隱藏的元素
   *
   * - 使這些元素透明化，避免影響樣式呈現
   * - 與選項有關，通常在需要渲染或計算 XPath/CSS selector 時才執行
   */
  toggleUnwrappedElems(wrapper, toShowSelector, toHideSelector, onReplace) {
    const doc = wrapper.ownerDocument;
    if (toShowSelector) {
      for (const elem of wrapper.querySelectorAll(toShowSelector)) {
        const rElem = doc.createElement(elem.getAttribute('tag'));
        for (const attr of elem.attributes) {
          const nodeName = attr.nodeName;
          if (!nodeName.startsWith('attr-')) {
            continue;
          }
          rElem.setAttribute(nodeName.slice(5), attr.nodeValue);
        }
        let child;
        while (child = elem.firstChild) {
          rElem.appendChild(child);
        }
        elem.replaceWith(rElem);
        onReplace && onReplace(elem, rElem);
      }
    }
    if (toHideSelector) {
      for (const elem of wrapper.querySelectorAll(toHideSelector)) {
        const rElem = doc.createElement('jc-t');
        rElem.setAttribute('tag', elem.nodeName.toLowerCase());
        for (const attr of elem.attributes) {
          rElem.setAttribute(`attr-${attr.nodeName}`, attr.nodeValue);
        }
        let child;
        while (child = elem.firstChild) {
          rElem.appendChild(child);
        }
        elem.replaceWith(rElem);
        onReplace && onReplace(elem, rElem);
      }
    }
  },

  /**
   * 切換直書模式
   */
  setVerticalMode(useVertical, classes) {
    const handlerModifiers = ['ctrlKey', 'altKey', 'shiftKey', 'metaKey'];

    const keyboardHandler = (event) => {
      if (handlerModifiers.some(x => !!event[x])) { return; }
      if (!this.panelElem.hidden) { return; }

      switch (event.key) {
        case 'PageUp':
          window.scrollBy(getPageHeight('X'), 0);
          event.preventDefault();
          break;
        case "PageDown":
          window.scrollBy(-getPageHeight('X'), 0);
          event.preventDefault();
          break;
        case 'End':
          window.scrollTo(-document.documentElement.scrollWidth, 0);
          event.preventDefault();
          break;
        case 'Home':
          window.scrollTo(0, 0);
          event.preventDefault();
          break;
      }
    };

    const getLineHeight = (elem) => {
      return parseInt(window.getComputedStyle(elem, null).lineHeight, 10) || 16;
    };

    const getPageHeight = (axis) => {
      switch (axis) {
        case 'X': {
          return document.documentElement.clientWidth;
        }
        case 'Y':
        default: {
          return document.documentElement.clientHeight;
        }
      }
    };

    // @TODO: allow scrolling a scrollbar in the content
    const mouseWheelHandler = (event) => {
      if (handlerModifiers.some(x => !!event[x])) { return; }
      if (event.deltaY === 0) { return; }
      if (!this.panelElem.hidden) { return; }

      let delta = event.deltaY;
      switch (event.deltaMode) {
        case WheelEvent.DOM_DELTA_LINE: {
          delta *= getLineHeight(event.target);
          break;
        }
        case WheelEvent.DOM_DELTA_PAGE: {
          delta *= getPageHeight('X');
          break;
        }
      }

      window.scrollBy(-delta, 0);
      event.preventDefault();
    };

    let isVertical;

    const setVerticalMode = jicheng.setVerticalMode = (useVertical, classes) => {
      if (useVertical) {
        classes.add('直書');
        if (!isVertical) {
          // some browsers defaults events like wheel to passive: true
          window.addEventListener('keydown', keyboardHandler, {passive: false});
          window.addEventListener('wheel', mouseWheelHandler, {passive: false});
        }
      } else {
        classes.delete('直書');
        if (isVertical) {
          window.removeEventListener('keydown', keyboardHandler);
          window.removeEventListener('wheel', mouseWheelHandler);
        }
      }

      isVertical = useVertical;
    };

    return setVerticalMode(useVertical, classes);
  },

  /**
   * 進階注疏校篩選
   */
  setAdvancedAnnotationFilter(...args) {
    const getCssText = (data) => {
      if (data === null || typeof data !== 'object') {
        data = {value: data};
      }
      try {
        return Object.entries(data.css).reduce((rv, [k, v]) => {
          v = this.tidyCssPropertyValue(k, v);
          if (v) {
            rv.push(`${k}: ${v};`);
          }
          return rv;
        }, []).join(' ');
      } catch (ex) {
        return '';
      }
    };

    const setAdvancedAnnotationFilter = (advancedAnnotationFilter, styleSheets) => {

      if (!advancedAnnotationFilter) { return; }
      const styles = [[], [], []];
      for (const key of ['注', '疏', '校']) {
        for (let ver in advancedAnnotationFilter[key]) {
          let cssText = getCssText(advancedAnnotationFilter[key][ver]);
          ver = CSS.escape(ver);
          if (!ver) {
            styles[0].push(`[data-render="book"].${key}-${ver}-接受 span[data-rev="${key}"]:not([data-ver])`);
            styles[1].push(`[data-render="book"].${key}-${ver}-拒絕 span[data-rev="${key}"]:not([data-ver])`);
            if (cssText) {
              styles[2].push(`\
[data-render="book"]:not(.${key}-${ver}-接受):not(.${key}-${ver}-拒絕) span[data-rev="${key}"]:not([data-ver]),
[data-render="book"].${key}-${ver}-接受 span[data-rev="${key}"]:not([data-ver]),
[data-render="book"].${key}-${ver}-拒絕 span[data-rev="${key}"]:not([data-ver]) {
  ${cssText}
}`);
            }
          }
          styles[0].push(`[data-render="book"].${key}-${ver}-接受 span[data-rev="${key}"][data-ver="${ver}"]`);
          styles[1].push(`[data-render="book"].${key}-${ver}-拒絕 span[data-rev="${key}"][data-ver="${ver}"]`);
          if (cssText) {
            styles[2].push(`\
[data-render="book"]:not(.${key}-${ver}-接受):not(.${key}-${ver}-拒絕) span[data-rev="${key}"][data-ver="${ver}"],
[data-render="book"].${key}-${ver}-接受 span[data-rev="${key}"][data-ver="${ver}"],
[data-render="book"].${key}-${ver}-拒絕 span[data-rev="${key}"][data-ver="${ver}"] {
  ${cssText}
}`);
          }
        }
      }
      styleSheets.push(`\
${styles[0].join(', ')} {
  color: unset;
}
${styles[1].join(', ')} {
  display: none;
}
${styles[2].join('\n')}`);
    };

    return setAdvancedAnnotationFilter(...args);
  },

  /**
   * 進階訂文篩選
   */
  setAdvancedVersionFilter(...args) {
    const getCssText = (data) => {
      if (data === null || typeof data !== 'object') {
        data = {value: data};
      }
      try {
        return Object.entries(data.css).reduce((rv, [k, v]) => {
          v = this.tidyCssPropertyValue(k, v);
          if (v) {
            rv.push(`${k}: ${v};`);
          }
          return rv;
        }, []).join(' ');
      } catch (ex) {
        return '';
      }
    };

    const setAdvancedVersionFilter = (advancedVersionFilter, styleSheets) => {
      if (!advancedVersionFilter) { return; }
      const styles = [[], [], [], []];
      for (let ver in advancedVersionFilter) {
        let cssText = getCssText(advancedVersionFilter[ver]);
        ver = CSS.escape(ver);
        if (!ver) {
          styles[0].push(`[data-render="book"].訂-${ver}-接受 span[data-rev="訂"]:not([data-ver])`);
          styles[1].push(`[data-render="book"].訂-${ver}-拒絕 span[data-rev="訂"]:not([data-ver])`);
          styles[2].push(`[data-render="book"].訂-${ver}-刪除 span[data-rev="訂"]:not([data-ver])`);
          if (cssText) {
            styles[3].push(`\
[data-render="book"]:not(.訂-${ver}-接受):not(.訂-${ver}-拒絕):not(.訂-${ver}-刪除) span[data-rev="訂"]:not([data-ver]),
[data-render="book"].訂-${ver}-接受 span[data-rev="訂"]:not([data-ver]),
[data-render="book"].訂-${ver}-拒絕 span[data-rev="訂"]:not([data-ver]),
[data-render="book"].訂-${ver}-刪除 span[data-rev="訂"]:not([data-ver]) {
  ${cssText}
}`);
          }
        }
        styles[0].push(`[data-render="book"].訂-${ver}-接受 span[data-rev="訂"][data-ver="${ver}"]`);
        styles[1].push(`[data-render="book"].訂-${ver}-拒絕 span[data-rev="訂"][data-ver="${ver}"]`);
        styles[2].push(`[data-render="book"].訂-${ver}-刪除 span[data-rev="訂"][data-ver="${ver}"]`);
        if (cssText) {
          styles[3].push(`\
[data-render="book"]:not(.訂-${ver}-接受):not(.訂-${ver}-拒絕):not(.訂-${ver}-刪除) span[data-rev="訂"][data-ver="${ver}"],
[data-render="book"].訂-${ver}-接受 span[data-rev="訂"][data-ver="${ver}"],
[data-render="book"].訂-${ver}-拒絕 span[data-rev="訂"][data-ver="${ver}"],
[data-render="book"].訂-${ver}-刪除 span[data-rev="訂"][data-ver="${ver}"] {
  ${cssText}
}`);
        }
      }
      styleSheets.push(`\
[data-render="book"] span[data-rev="訂"][data-ver]:not([data-ver="*"]) {
  display: unset;
}
[data-render="book"] span[data-rev="訂"][data-ver="*"] {
  text-decoration: unset;
}
${styles[0].join(', ')} {
  color: unset;
}
${styles[1].join(', ')} {
  display: none;
}
${styles[2].join(', ')} {
  text-decoration: line-through;
}
${styles[3].join('\n')}`);
    };

    return setAdvancedVersionFilter(...args);
  },

  /**
   * 切換字體
   */
  setFontMode(fontFamily, styleSheets) {
    if (!fontFamily) { return; }

    let fontImport = '';
    switch (fontFamily) {
      case "default-ext":
        fontFamily = `sans-serif, TW-Sung-local, TW-Kai-local, HanaMin-local, HanaMinA, HanaMinB`;
        fontImport = `\
@import "https://jicheng.tw/hanzi/common/webfont/HanaMinA/HanaMinA.css";
@import "https://jicheng.tw/hanzi/common/webfont/HanaMinB/HanaMinB.css";
`;
        break;
      case "hanamin":
        fontFamily = `HanaMin-local, HanaMinA, HanaMinB, serif`;
        fontImport = `\
@import "https://jicheng.tw/hanzi/common/webfont/HanaMinA/HanaMinA.css";
@import "https://jicheng.tw/hanzi/common/webfont/HanaMinB/HanaMinB.css";
`;
        break;
      case "twsung":
        fontFamily = `TW-Sung-local, TW-Sung, TW-Sung-Ext-B, serif, HanaMin-local`;
        fontImport = `\
@import "https://jicheng.tw/hanzi/common/webfont/TW-Sung-98_1/TW-Sung-98_1.css";
@import "https://jicheng.tw/hanzi/common/webfont/TW-Sung-Ext-B-98_1/TW-Sung-Ext-B-98_1.css";
`;
        break;
      case "twkai":
        fontFamily = `TW-Kai-local, TW-Kai, TW-Kai-Ext-B, sans-serif, HanaMin-local`;
        fontImport = `\
@import "https://jicheng.tw/hanzi/common/webfont/TW-Kai-98_1/TW-Kai-98_1.css";
@import "https://jicheng.tw/hanzi/common/webfont/TW-Kai-Ext-B-98_1/TW-Kai-Ext-B-98_1.css";
`;
        break;
    }

    styleSheets.unshift(fontImport); // @import must go first
    styleSheets.push(`\
.main, #page-panel-tabpanel-toc {
  font-family: ${fontFamily};
}`);
  },

  setFontSize(fontSize, styleSheets) {
    if (Number.isFinite(fontSize)) {
      styleSheets.push(`html { font-size: ${fontSize}px; }`);
    }
  },

  highlightText(...args) {
    const REGEX_PATTERN = /^\/(.*)\/([A-Za-z]*?)$/;
    const highlightText = jicheng.highlightText = (patterns, options, elem = this.mainElem) => {
      let regexes;
      try {
        regexes = patterns.map(pattern => {
          try {
            if (REGEX_PATTERN.test(pattern)) {
              return new RegExp(RegExp.$1, RegExp.$2);
            }
            return new RegExp(this.escapeRegExp(pattern));
          } catch (ex) {
            throw new Error(`${pattern}: ${ex.message}`);
          }
        });
      } catch (ex) {
        console.error(`Unable to highlight: ${ex.message}`);
        return;
      }

      // selector 比對 textNode.parentNode
      const excludeSelectors = [];
      if (options.useAncient) {
        excludeSelectors.push('[data-rev="今版"]');
      } else {
        excludeSelectors.push('[data-rev="古版"]');
      }
      switch (options.useZhu) {
        case "reject":
          excludeSelectors.push('span[data-rev="注"]');
          break;
      }
      switch (options.useShu) {
        case "reject":
          excludeSelectors.push('span[data-rev="疏"]');
          break;
      }
      switch (options.useJiao) {
        case "reject":
          excludeSelectors.push('span[data-rev="校"]');
          break;
      }
      switch (options.useDing) {
        case "accept":
          excludeSelectors.push('span[data-rev="訂"][data-ver]');
          break;
        case "reject":
          excludeSelectors.push('span[data-rev="訂"]:not([data-ver="*"])');
          break;
        case "diff":
        default:
          excludeSelectors.push('span[data-rev="訂"][data-ver]:not([data-ver="*"])');
          break;
      }

      this.mark(elem, regexes, {
        acrossElements: true,
        exclude: excludeSelectors,
      });
    };
    return highlightText(...args);
  },

  mark(...args) {
    const SUPPPORTED_COLORS = 8;
    const mark = this.mark = (elem, regexes, {acrossElements = false, exclude = []} = {}) => {
      if (!Array.isArray(regexes)) {
        regexes = [regexes];
      }
      regexes.forEach((regex, kwIndex) => {
        const index = kwIndex % SUPPPORTED_COLORS;
        new Mark(elem).markRegExp(regex, {
          acrossElements,
          exclude,
          className: `kw${index}`,
          each: (elem) => {
            elem.setAttribute('tabindex', '0');
          },
        });
      });
    };
    return mark(...args);
  },

  generateToc() {
    const titleElems = this.titleElems = Array.from(this.mainElem.querySelectorAll('[data-sec="h1"], [data-sec="h2"], [data-sec="h3"], [data-sec="h4"], [data-sec="h5"], [data-sec="h6"]'))
        .filter(x => !x.closest('main > header'));

    const root = document.querySelector('#page-panel-tabpanel-toc');
    let prevElem = root;
    let prevLevel = titleElems.reduce((curValue, titleElem) => {
      const level = parseInt(titleElem.getAttribute('data-sec').match(/(\d+)$/)[1], 10);
      return Math.min(curValue, level);
    }, Infinity);

    // 全文
    {
      const ol = document.createElement('ol');
      root.appendChild(ol);
      const li = document.createElement('li');
      ol.appendChild(li);

      const a = document.createElement('a');
      a.href = '#';
      a.textContent = '（全文）';
      a.addEventListener('click', (event) => {
        this.loadSection(null);
      });
      li.appendChild(a);
      this.mapTitleElemToTocElem.set(null, a);

      prevElem = li;
    }

    // 首節
    {
      const li = document.createElement('li');
      prevElem.parentNode.appendChild(li);

      const a = document.createElement('a');
      a.href = '#?t=.';
      a.textContent = '（首節）';
      a.addEventListener('click', (event) => {
        this.loadSection(this.mainElem);
      });
      li.appendChild(a);
      this.mapTitleElemToTocElem.set(this.mainElem, a);

      prevElem = li;
    }

    const map = new WeakMap();
    for (const titleElem of titleElems) {
      const level = parseInt(titleElem.getAttribute('data-sec').match(/(\d+)$/)[1], 10);
      const li = document.createElement('li');

      if (level > prevLevel) {
        let curElem = prevElem;
        while (level > prevLevel) {
          curElem = curElem.appendChild(document.createElement('ol'));
          if (level - prevLevel > 1) {
            curElem = curElem.appendChild(document.createElement('li'));
          }
          prevLevel++;
        }
        curElem.appendChild(li);
      } else if (level < prevLevel) {
        let curElem = prevElem;
        while (prevLevel > level) {
          curElem = curElem.parentNode.parentNode;
          prevLevel--;
        }
        curElem.parentNode.appendChild(li);
      } else {
        prevElem.parentNode.appendChild(li);
      }

      const a = document.createElement('a');
      a.textContent = this.getTocText(titleElem);
      a.href = '#?t=' + this.getXPath(titleElem, this.mainElem, map);
      a.addEventListener('click', (event) => {
        this.loadSection(titleElem);
      });
      li.appendChild(a);
      this.mapTitleElemToTocElem.set(titleElem, a);

      prevElem = li;
      prevLevel = level;
    }

    // 標示（全文）項
    const selectedTocElem = this.mapTitleElemToTocElem.get(null);
    if (selectedTocElem) {
      selectedTocElem.classList.add('current');
    }
  },

  /**
   * 載入章節
   *
   * @param {*} titleElem - 欲載入的章節元素
   *     - falsy: 顯示全文, mainElem: 顯示首節, 其他: 顯示該標題元素所在節
   */
  loadSection(titleElem, {
    tocOnly = false,
    closePanel = true,
  } = {}) {
    // 關閉工具欄
    if (closePanel) {
      this.togglePanel(false);
    }

    const selectedTocElem = this.mapTitleElemToTocElem.get(titleElem || null);

    // titleElem 不是有效的章節標題
    if (!selectedTocElem) {
      throw new Error('Attempt to load an invalid section.');
    }

    // 標示選擇的目錄項
    for (const elem of document.querySelectorAll('#page-panel-tabpanel-toc a.current')) {
      elem.classList.remove('current');
    }
    selectedTocElem.classList.add('current');

    if (tocOnly) {
      return;
    }

    // 定位模式時，特別處理
    if (this.options['tocMode'] === 'locate') {
      // 顯示全文
      if (this.mainElem.hidden) {
        this.mainElemSectioned.hidden = true;
        this.showRenderingElem(this.mainElem);
      }

      if (titleElem) {
        this.locateElement(titleElem);
      } else {
        window.scrollTo(0, 0);
      }

      return;
    }

    if (!titleElem) {
      // 顯示全文
      if (this.mainElem.hidden) {
        this.mainElemSectioned.hidden = true;
        this.showRenderingElem(this.mainElem);
      }
    } else {
      let prevTitleElem = null;
      let nextTitleElem = null;

      if (titleElem === this.mainElem) {
        // 首節
        nextTitleElem = this.titleElems[0] || nextTitleElem;
      } else {
        const idx = this.titleElems.indexOf(titleElem);
        prevTitleElem = this.titleElems[idx - 1] || prevTitleElem;
        nextTitleElem = this.titleElems[idx + 1] || nextTitleElem;
      }

      // 設定要顯示的元素
      const classes = new Set([...this.mainElem.classList, 'sectioned']);
      this.mainElem.hidden = true;
      this.mainElemSectioned.hidden = true;
      this.mainElemSectioned.className = [...classes].join(' ');
      this.mainElemSectioned.textContent = '';
      this.mainElemSectioned.appendChild(this.cloneFragmentBetween(this.mainElem, titleElem, nextTitleElem));

      // 建立「上一節」「下一節」
      {
        const footerElem = this.mainElemSectioned.appendChild(document.createElement('footer'));

        if (prevTitleElem) {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '上一節：';

          const a = div.appendChild(document.createElement('a'));
          a.innerHTML = this.mapTitleElemToTocElem.get(prevTitleElem).innerHTML;
          a.href = '#?t=' + this.getXPath(prevTitleElem, this.mainElem);
          a.addEventListener('click', (event) => {
            this.loadSection(prevTitleElem);
          });
        } else if (titleElem !== this.mainElem) {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '上一節：';

          const a = div.appendChild(document.createElement('a'));
          a.textContent = '（首節）';
          a.href = '#?t=.';
          a.addEventListener('click', (event) => {
            this.loadSection(this.mainElem);
          });
        } else {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '上一節：無';
        }

        if (nextTitleElem) {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '下一節：';

          const a = div.appendChild(document.createElement('a'));
          a.innerHTML = this.mapTitleElemToTocElem.get(nextTitleElem).innerHTML;
          a.href = '#?t=' + this.getXPath(nextTitleElem, this.mainElem);
          a.addEventListener('click', (event) => {
            this.loadSection(nextTitleElem);
          });
        } else {
          const div = footerElem.appendChild(document.createElement('div'));
          div.textContent = '下一節：無';
        }
      }

      // 顯示 mainElemSectioned，因其內容可能變動，一律重新 dispatchEvent
      this.showRenderingElem(this.mainElemSectioned);
    }

    window.scrollTo(0, 0);
  },

  /**
   * 取得包含指定元素的章節元素
   */
  getParentSection(elem) {
    let curTitleElem = null;
    for (const titleElem of this.titleElems) {
      if (titleElem.compareDocumentPosition(elem) & Node.DOCUMENT_POSITION_PRECEDING) {
        break;
      }
      curTitleElem = titleElem;
    }
    return curTitleElem;
  },

  getTocText(elem) {
    const cloneElem = elem.cloneNode(true);

    for (const elem of cloneElem.querySelectorAll('[data-sec=""], [data-rev="古版"]')) {
      elem.remove();
    }

    setHanzi: {
      if (!(jchanzi && jchanzi.processPage)) {
        break setHanzi;
      }

      jchanzi.unprocessElement(cloneElem);
    }

    return cloneElem.textContent;
  },

  cloneFragmentBetween(mainElem, startElem, endElem) {
    const range = new Range();
    if (startElem !== mainElem) {
      range.setStartBefore(startElem);
    } else {
      range.setStart(mainElem, 0);
    }
    if (endElem) {
      range.setEndBefore(endElem);
    } else {
      range.setEnd(mainElem, mainElem.childNodes.length);
    }
    const ca = range.commonAncestorContainer;

    const wrappers = [];
    let tempNode = ca;
    while (tempNode && tempNode !== mainElem) {
      wrappers.unshift(tempNode);
      tempNode = tempNode.parentNode;
    }

    const frag = document.createDocumentFragment();
    tempNode = frag;
    for (const wrapper of wrappers) {
      tempNode = tempNode.appendChild(wrapper.cloneNode(false));
    }

    tempNode.appendChild(range.cloneContents());

    return frag;
  },

  // @TODO: 若 target 的 parent 為 pre，Chrome 取得的 domRect 數值會不正確
  locateElement(target) {
    if (!target) { return; }

    // target 被隱藏時無法定位，建立暫時節點作為參照 
    const getUppermostHiddenParent = (node) => {
      let curNode = node;
      while (!node.offsetParent) {
        curNode = node;
        node = node.parentNode;
      }
      return curNode;
    };

    let ref = target;
    if (!ref.offsetParent) {
      ref = getUppermostHiddenParent(ref);
      ref = ref.parentNode.insertBefore(document.createElement('jc-locate'), ref);
    }

    if (!document.documentElement.classList.contains('直書')) {
      const domRect = ref.getBoundingClientRect();
      window.scrollBy(domRect.left, domRect.top);
    } else {
      const domRect = ref.getBoundingClientRect();
      const viewportWidth = document.documentElement.clientWidth;
      const deltaX = domRect.left + domRect.width - viewportWidth;
      window.scrollBy(deltaX, domRect.top);
    }

    if (ref !== target) {
      ref.remove();
    }
  },

  loadOptionsToPanel(keys = Object.keys(this.options)) {
    for (const key of keys) {
      const elem = document.querySelector(`#${key}`);
      if (elem === null) {
        continue;
      }

      elem.value = this.options[key];
    }
  },

  saveOptionsFromPanel(keys = Object.keys(this.options)) {
    for (const key of keys) {
      const elem = document.querySelector(`#${key}`);
      if (elem === null) {
        continue;
      }

      if (elem.matches('input[type="number"]')) {
        this.options[key] = elem.valueAsNumber;
      } else {
        this.options[key] = elem.value;
      }
    }
  },

  loadLocalStorage(keys = SYNCED_OPTIONS) {
    try {
      for (const key of keys) {
        var value = sessionStorage.getItem(`jc_${key}`);
        if (value !== null) {
          this.options[key] = value;
          continue;
        }

        var value = localStorage.getItem(`jc_${key}`);
        if (value !== null) {
          this.options[key] = value;
          continue;
        }
      }
    } catch (ex) {
      console.error(ex);
    }
  },

  saveLocalStorage(keys = SYNCED_OPTIONS) {
    try {
      for (const key of keys) {
        if (this.options[key] !== undefined) {
          localStorage.setItem(`jc_${key}`, this.options[key]);
          sessionStorage.setItem(`jc_${key}`, this.options[key]);
        }
      }
    } catch (ex) {
      console.error(ex);
    }
  },

  loadUrlParams(object = this.options, schema = URL_PARAMS_SCHEMA, schemaHandler = URL_PARAMS_HANDLER) {
    const query = location.hash.slice(1);
    if (!query.startsWith('?')) { return object; }
    const params = new URLSearchParams(query);
    for (let [key, {name, type}] of Object.entries(schema)) {
      if (!params.has(key)) { continue; }
      name = name || key;
      if (type.endsWith('[]')) {
        type = type.slice(0, -2);
        const handler = schemaHandler[type] || schemaHandler['string'];
        object[name] = params.getAll(key).map(handler);
      } else {
        const handler = schemaHandler[type] || schemaHandler['string'];
        object[name] = handler(params.get(key));
      }
    }
    return object;
  },

  updateRuntimeOptions(options = this.options) {
    let useAncient = false;
    if (this.mainElem.getAttribute('data-render') === 'book') {
      let mode = options['useAncient'];

      switch (mode) {
        case "true":
          useAncient = true;
          break;
        case "false":
          useAncient = false;
          break;
        default: {
          // 1. 元資料有定義者（"古版", "今版"），取之
          // 2. 未定義者，預設為今版
          const meta = (this.loadBookMeta(this.mainElem)['版式'] || [undefined])[0];
          if (meta === '古版') {
            useAncient = true;
          } else if (meta === '今版') {
            useAncient = false;
          } else {
            useAncient = false;
          }
          break;
        }
      }
    }

    let useZhu = "diff";
    if (this.mainElem.getAttribute('data-render') === 'book') {
      let mode = options['useZhu'];

      switch (mode) {
        case "diff":
        case "accept":
        case "reject":
          useZhu = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useZhu = "diff";
          break;
      }
    }

    let useShu = "diff";
    if (this.mainElem.getAttribute('data-render') === 'book') {
      let mode = options['useShu'];

      switch (mode) {
        case "diff":
        case "accept":
        case "reject":
          useShu = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useShu = "diff";
          break;
      }
    }

    let useJiao = "diff";
    if (this.mainElem.getAttribute('data-render') === 'book') {
      let mode = options['useJiao'];

      switch (mode) {
        case "diff":
        case "accept":
        case "reject":
          useJiao = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useJiao = useAncient ? "reject" : "diff";
          break;
      }
    }

    let useDing = "diff";
    if (this.mainElem.getAttribute('data-render') === 'book') {
      let mode = options['useDing'];

      switch (mode) {
        case "diff":
        case "accept":
        case "reject":
          useDing = mode;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useDing = useAncient ? "reject" : "diff";
          break;
      }
    }

    let useVertical = false;
    {
      let mode = options['useVertical'];

      switch (mode) {
        case "true":
          useVertical = true;
          break;
        case "false":
          useVertical = false;
          break;
        default: {
          // 1. 元資料有定義者（"橫書", "直書"），取之
          // 2. 未定義者，今版預設橫書，古版預設直書
          const meta = (this.loadBookMeta(this.mainElem)['直書'] || [undefined])[0];
          if (meta === '直書') {
            useVertical = true;
          } else if (meta === '橫書') {
            useVertical = false;
          } else {
            useVertical = !!useAncient;
          }
          break;
        }
      }
    }

    let fontFamily = false;
    {
      let mode = options['useFont'];

      switch (mode) {
        case "default":
          try {
            jchanzi.conf.fontName = '';
          } catch (ex) {
            console.error(ex);
          }
          fontFamily = false;
          break;
        case "default-ext":
          try {
            jchanzi.conf.fontName = '';
          } catch (ex) {
            console.error(ex);
          }
          fontFamily = mode;
          break;
        case "hanamin":
          try {
            jchanzi.conf.fontName = 'm';
          } catch (ex) {
            console.error(ex);
          }
          fontFamily = mode;
          break;
        case "twsung":
          try {
            jchanzi.conf.fontName = 's';
          } catch (ex) {
            console.error(ex);
          }
          fontFamily = mode;
          break;
        case "twkai":
          try {
            jchanzi.conf.fontName = 'k';
          } catch (ex) {
            console.error(ex);
          }
          fontFamily = mode;
          break;
        case "custom":
          try {
            jchanzi.conf.fontName = '';
          } catch (ex) {
            console.error(ex);
          }
          fontFamily = this.tidyCssPropertyValue('font-family', options['useFontCustom']) || 'unset';
          break;
        default:
          // @TODO: 根據情況自動判斷
          try {
            jchanzi.conf.fontName = '';
          } catch (ex) {
            console.error(ex);
          }
          if (useAncient) {
            // 古版時使用中文字體，因英文字體的標點符號有時不能轉90度排
            fontFamily = `"DroidSansFallback", "WenQuanYi Zen Hei", "Heiti TC", "PMingLiU", "PMingLiU-ExtB", sans-serif`;
          } else {
            fontFamily = false;
          }
          break;
      }
    }

    let fontSize;
    {
      fontSize = parseFloat(options['fontSize']);
    }

    let useHanzi = true;
    {
      let mode = options['useHanzi'];

      switch (mode) {
        case "basic":
        case "advanced":
          useHanzi = mode;
          break;
        case "false":
          useHanzi = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          useHanzi = "advanced";
          break;
      }
    }

    let prettify = false;
    {
      let mode = options['prettify'];

      switch (mode) {
        case "true":
          prettify = true;
          break;
        case "false":
          prettify = false;
          break;
        default:
          // @TODO: 根據情況自動判斷
          prettify = false;
          break;
      }
    }

    Object.assign(this.runtimeOptions, options, {
      useAncient,
      useZhu,
      useShu,
      useJiao,
      useDing,
      useVertical,
      fontFamily,
      fontSize,
      useHanzi,
      prettify,
    });
  },
};

document.addEventListener('touchstart', (event) => {
  jicheng.lastTouchElement = event.target;
});

document.addEventListener('contextmenu', (event) => {
  if (event.target !== jicheng.lastTouchElement) { return; }

  // 避免影響使用者長按選取複製
  if (event.target.matches('input[type="text"], input[type="number"], textarea')) { return; }

  const elem = event.target.closest('[aria-description]');
  const title = elem.getAttribute('aria-description');
  if (elem && title) {
    alert(title);
    event.preventDefault();
  }
});

document.addEventListener('DOMContentLoaded', jicheng.init.bind(jicheng));

return jicheng;

}));
